﻿using System;
using PuzzledTime.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace PuzzledTime.Transitions
{
    [RequireComponent(typeof(Image))]
    
    public class TransitionGui : Gui<TransitionGui>
    {
        private static readonly int LevelResetAnimation = Animator.StringToHash("LevelReset");
        private static readonly int GuiAnimation = Animator.StringToHash("Gui");

        public void Transition(TransitionType type, bool value)
        {
            switch (type)
            {
                case TransitionType.LevelReset:
                    Animator.SetBool(LevelResetAnimation, value);
                    break;
                case TransitionType.Gui:
                    Animator.SetBool(GuiAnimation, value);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
    }

    public enum TransitionType
    {
        LevelReset,
        Gui
    }
}

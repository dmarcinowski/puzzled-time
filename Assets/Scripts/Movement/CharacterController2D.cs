﻿using UnityEngine;
using UnityEngine.Events;

namespace PuzzledTime.Movement
{
    public class CharacterController2D : MonoBehaviour
    {
        public static bool canMove = true;
        [Header("Movement")]
        [SerializeField] private float movementSpeed;
        [SerializeField] [Range(0, 1)] private float crouchSpeed;
        [SerializeField] private float jumpForce;
        [SerializeField] private float movementSmoothing;
        [SerializeField] private bool airControl;

        public float MovementSpeed => movementSpeed;
        
        [Header("Variables")]
        [SerializeField] private LayerMask groundLayer;
        [SerializeField] private Collider2D walkCollider;
        [SerializeField] private Collider2D standCollider;
        [SerializeField] private Collider2D sneakCollider;
        [SerializeField] private Transform groundCheck;
        [SerializeField] private Transform ceilingCheck;
        [SerializeField] private Transform wallCheck;
        [SerializeField] private Transform wallCheckCrouch;

        public Transform GroundCheck => groundCheck;
        
        [Header("Events")] [Space]
        public UnityEvent onLandEvent;
        public UnityEvent<bool> onCrouchEvent;
        public UnityEvent<bool> onHitWallEvent;
        
        private Rigidbody2D m_rigidbody;
        private Vector3 m_velocity = Vector3.zero;
        private bool m_isCrouching = false;
        private bool m_facingRight = true;

        private bool m_grounded;
        private const float GroundRadius = 0.2f;
        private const float CeilingRadius = 0.2f;
        private const float WallSize = 1.2f;

        public bool Grounded => m_grounded;

        private void Awake()
        {
            onLandEvent ??= new UnityEvent();
            onCrouchEvent ??= new UnityEvent<bool>();
            onHitWallEvent ??= new UnityEvent<bool>();

            m_rigidbody = gameObject.GetComponent<Rigidbody2D>();
        }

        private void FixedUpdate()
        {
            if (canMove)
            {
                // Check if player is on ground
                var wasGrounded = m_grounded;
                m_grounded = false;

                var groundColliders = Physics2D.OverlapCircleAll(groundCheck.position, GroundRadius, groundLayer);
                foreach (var groundCollider in groundColliders)
                {
                    if (groundCollider.gameObject == gameObject)
                        continue;

                    m_grounded = true;
                    if (!wasGrounded) onLandEvent.Invoke();
                }
            }
           
        }
        
        public void Move(float move, bool crouch, bool jump)
        {
            if(canMove)
            {
                // Make player unable to un-crouch if under ceiling
                if (!crouch)
                {
                    var underCeiling = Physics2D.OverlapCircle(ceilingCheck.position, CeilingRadius, groundLayer);
                    if (underCeiling) crouch = true;
                }

                // If player has hit the wall fire the onHitWallEvent
                var hasHitWall = Physics2D.OverlapBox(
                    crouch ? wallCheckCrouch.position : wallCheck.position,
                    new Vector2(WallSize, WallSize),
                    0f, groundLayer);
                onHitWallEvent.Invoke(hasHitWall);

                // Move the player
                if (m_grounded || airControl)
                {
                    IsCrouching(ref move, crouch);

                    var rigidbodyVelocity = m_rigidbody.velocity;

                    Vector3 targetVelocity = new Vector2(move * movementSpeed * transform.localScale.y, rigidbodyVelocity.y);
                    m_rigidbody.velocity = Vector3.SmoothDamp(rigidbodyVelocity, targetVelocity, ref m_velocity, movementSmoothing);

                    if (move > 0 && !m_facingRight)
                        Flip(m_facingRight);
                    else if (move < 0 && m_facingRight)
                        Flip(m_facingRight);
                }

                // Jump by adding y force
                if (m_grounded && jump)
                {
                    m_rigidbody.AddForce(new Vector2(0f, jumpForce * transform.localScale.y));
                    m_grounded = false;
                }
            }
            else
            {
                m_rigidbody.velocity = new Vector2(0,0);
            }
         
        }

        public void Flip(bool facingRight)
        {
            m_facingRight = !facingRight;

            var theScale = transform.localScale;
            theScale.x *= -1;
            
            gameObject.transform.localScale = theScale;
        }

        private void IsCrouching(ref float move, bool value)
        {
            if (value)
            {
                move *= crouchSpeed;
                
                if (!m_isCrouching)
                {
                    m_isCrouching = true;
                    onCrouchEvent.Invoke(true);
                }
                
                if (walkCollider != null)
                    walkCollider.enabled = false;
                if (standCollider != null)
                    standCollider.enabled = false;
                if (sneakCollider != null)
                    sneakCollider.enabled = true;
            }
            else
            {
                if (m_isCrouching)
                {
                    m_isCrouching = false;
                    onCrouchEvent.Invoke(false);
                }

                if (walkCollider != null)
                    walkCollider.enabled = true;
                if (standCollider != null)
                    standCollider.enabled = true;
                if (sneakCollider != null)
                    sneakCollider.enabled = false;
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireCube(m_isCrouching ? wallCheckCrouch.position : wallCheck.position, new Vector3(WallSize, WallSize, 0));
            Gizmos.DrawWireSphere(groundCheck.position, GroundRadius);
            Gizmos.DrawWireSphere(ceilingCheck.position, CeilingRadius);
        }

        public static void DisableMovement(bool value)
        {
            canMove = value;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using PuzzledTime.Movement;
using PuzzledTime.Interaction;
using PuzzledTime.Inventory.Item;
using UnityEngine;

namespace PuzzledTime
{
    public class Chest : Interactable
    {
        [SerializeField] private ItemObject chestItem;

        private Animator chestAnimator;

        protected override string GizmoIcon => "ItemTrigger.png";

        private void Start() 
        {
            chestAnimator = GetComponent<Animator>();
            chestAnimator.keepAnimatorControllerStateOnDisable = true;
        }

        public override void Trigger()
        {
            if(Enabled) 
            {
                Enabled = false;
                chestAnimator.SetBool("openChest", true);
                Managers.AudioManager.Instance.PlaySound("chest");
                StartCoroutine(InteractionEnabler());
            }
        }

        IEnumerator InteractionEnabler() 
        {
            CharacterController2D.canMove = false;
            yield return new WaitForSeconds(1);
            Inventory.InventoryManager.Instance.AddItem(chestItem.Item);
            CharacterController2D.canMove = true;
        }
    }
}

﻿using PuzzledTime.Utilities;
using PuzzledTime.Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PuzzledTime.Interaction
{
    [RequireComponent(typeof(Image))]
    
    public class InteractionGui : Gui<InteractionGui>
    {
        [SerializeField] private TextMeshProUGUI letter;
        [SerializeField] private Canvas gameCanvas;
        
        private bool canBeShown = true;
        private RectTransform m_rectTransform;
        private RectTransform m_canvasRectTransform;

        protected override void Awake()
        {
            base.Awake();
            Show(false);
            
            m_rectTransform = gameObject.GetComponent<RectTransform>();
            m_canvasRectTransform = gameCanvas.GetComponent<RectTransform>();
        }

        public void SetLetter(KeyCode keyCode)
        {
            letter.text = keyCode.ToString();
        }

        protected override void Update()
        {
            if (canBeShown == false)
            {
                gameObject.SetActive(false);
                return;
            }
        }

        public void UpdatePosition(Transform point)
        {
            var screenPos = Camera.main.WorldToScreenPoint(point.position);
            
            var sizeDelta = m_canvasRectTransform.sizeDelta;
            var x = sizeDelta.x * (screenPos.x / Screen.width) - sizeDelta.x * 0.5f;
            var y = sizeDelta.y * (screenPos.y / Screen.height) - sizeDelta.y * 0.5f;

            m_rectTransform.anchoredPosition = new Vector2(x, y);
        }

        public override void Show(bool value)
        {
            isOpened = value;
            gameObject.SetActive(value);
        }

        public void Disable(bool value)
        {
            canBeShown = !value;
            InteractionManager.Instance.gameObject.SetActive(!value);
        }
    }
}

﻿using UnityEngine;

namespace PuzzledTime.Interaction
{
    [RequireComponent(typeof(BoxCollider2D))]
    
    public abstract class Interactable : MonoBehaviour
    {
        [Header("Interaction")]
        [SerializeField] private bool automatic;
        
        public bool Automatic => automatic;
        [field:SerializeField]public bool Enabled { get; set; } = true;

        protected abstract string GizmoIcon { get; }

        public abstract void Trigger();

        protected virtual void OnDrawGizmos() => Gizmos.DrawIcon(transform.position, GizmoIcon);
    }
}

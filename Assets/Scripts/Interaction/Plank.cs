using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PuzzledTime
{
    public class Plank : MonoBehaviour
    {
        [SerializeField] private GameObject connectedRigidbody;
        private Rigidbody2D plank;

        private void Start()
        {
            plank = GetComponent<Rigidbody2D>();
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            connectedRigidbody.SetActive(false);
            StartCoroutine(WaitForDisappear());
        }

        private IEnumerator WaitForDisappear() 
        {
            yield return new WaitForSeconds(0.5F);
            plank.bodyType = RigidbodyType2D.Static;
        }
    }
}

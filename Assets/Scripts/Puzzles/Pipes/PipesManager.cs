using UnityEngine;
using PuzzledTime.Inventory.Item;
using System.Collections;

namespace PuzzledTime.Puzzle
{
    public class PipesManager : PuzzleTrigger
    {
        public GameObject[] Pipes;
        [SerializeField] private ItemObject itemToGive;
        [SerializeField] private Canvas puzzleCanvas;

        [SerializeField] private int totalPipes = 27;
        [SerializeField] private int goodPipes = 0;

        private void Start()
        {
            var Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
            puzzleCanvas.worldCamera = Camera;
            puzzleCanvas.sortingLayerName = "Puzzle";
            var parent = transform.GetChild(0).GetChild(1);
            for(var i = 0; i< Pipes.Length; i++)
            {
                Pipes[i] = parent.GetChild(i).gameObject;
            }
        }

        protected override void Update()
        {
            base.Update();
            if (goodPipes == totalPipes)
            { 
                goodPipes = 0;
                solved = true;
                StartCoroutine(addItem());
                foreach(var pipes in Pipes)
                {
                    pipes.GetComponent<PipesPuzzle>().staticPipe = true;
                }

            }
        }

        IEnumerator addItem()
        {
            yield return new WaitForSeconds(3f);
            Inventory.InventoryManager.Instance.AddItem(itemToGive.Item);
        }
        public void GoodPipe()
        {
            goodPipes++;
        }
        public void WrongPipe()
        {
            goodPipes--;
        }
    }
}

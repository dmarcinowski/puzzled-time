using UnityEngine;
using UnityEngine.EventSystems;

namespace PuzzledTime.Puzzle
{
    public class PipesPuzzle : MonoBehaviour
    {
        readonly float[] rotations = { 0, 90, 180, 270, -90, -180 };

        [SerializeField] public bool staticPipe = false;
        [SerializeField] private bool correct = false;

        [SerializeField] private int[] correctAnswers;
        [SerializeField] private GameObject grid;

        private PipesManager _pipesManager;

        private void Awake()
        {
            _pipesManager = grid.GetComponent<PipesManager>();
        }

        void Start()
        {
            if(!staticPipe)
            {
                var random = Random.Range(0, rotations.Length);
                transform.eulerAngles = new Vector3(0, 0, rotations[random]);
                Check();
            }
            

        }
        private void Check()
        {
            foreach (var answer in correctAnswers)
            {   
                if (answer == Mathf.RoundToInt(transform.rotation.eulerAngles.z) && !correct)
                {
                    correct = true;
                    _pipesManager.GoodPipe();
                    return;
                }
                else if (answer != Mathf.RoundToInt(transform.rotation.eulerAngles.z) && correct)
                {
                    correct = false;
                    _pipesManager.WrongPipe();
                }
            }
        }

        private void OnMouseDown()
        {
           if(!staticPipe)
            {
                transform.Rotate(new Vector3(0, 0, 90));
                Managers.AudioManager.Instance.PlaySound("pipe");
            }
                
            
            Check();

        }
    }
}

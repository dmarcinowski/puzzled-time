﻿using PuzzledTime.Interaction;
using UnityEngine;
using PuzzledTime.Managers;
using PuzzledTime.Level;
using PuzzledTime.Inventory;
using System.Collections;
using PuzzledTime.Dialogue;
using PuzzledTime.Inventory.Item;

namespace PuzzledTime.Puzzle
{
    public abstract class PuzzleTrigger : Interactable
    {
        [Header("Puzzle information")]
        [SerializeField] private GameObject background;
        [SerializeField] protected bool solved;
        [SerializeField] protected bool requiredItem;
        [SerializeField] protected bool requiredOnlyOneItem;
        [SerializeField] protected ItemObject[] listOfItem;
        [SerializeField] protected DialogueObject dialogueNotHaveItem;

        protected override string GizmoIcon => "PuzzleTrigger.png";

        protected virtual void Update()
        {
            if (PuzzleGui.Instance.IsOpened == false)
            {
                InteractionGui.Instance.Disable(false);
                
            }

            if (solved)
            {
                Enabled = false;
                LevelManager.Instance.exitDoor.GetComponent<RoomTrigger>().SolvePuzzle();
                StartCoroutine(Disable());

                foreach (var item in listOfItem)
                {
                   InventoryManager.Instance.RemoveItem(item.Item);
                }
            }

        }

        IEnumerator Disable()
        {
            yield return new WaitForSeconds(3f);
            PuzzleGui.Instance.Show(false);
            InteractionGui.Instance.Disable(false);
        }
        protected bool CheckIfHasItem()
        {
            if (requiredOnlyOneItem)
            {
                foreach (var element in listOfItem)
                {
                    if (element.Item.Id == InventoryManager.Instance.GetCurrentId())
                        return true;
                }
                return false;
            }
            else
            {
                foreach (var element in listOfItem)
                {
                    bool haveSpecificItem = false;
                    for (var i = 0; i < 8; i++)
                    {
                        if (element.Item.Id == InventoryManager.Instance.GetItemId(i))
                            haveSpecificItem = true;

                    }
                    if (!haveSpecificItem)
                    {
                        return false;
                    }
                }
                return true;

            }
           

        }
        protected virtual void OnPuzzleActive()
        {
            return;
        }
        public override void Trigger()
        {
            if(requiredItem && CheckIfHasItem() || !requiredItem)
            {
                InteractionGui.Instance.Disable(true);
                PuzzleGui.Instance.SetBackground(background);
                PuzzleGui.Instance.Show(true);
                OnPuzzleActive();
            }
            else 
            {
                InteractionGui.Instance.Disable(true);
                DialogueManager.Instance.StartDialogue(dialogueNotHaveItem.Dialogue);
              
            }
        }
    }
}

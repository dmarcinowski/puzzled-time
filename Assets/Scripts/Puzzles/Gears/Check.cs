using UnityEngine;

namespace PuzzledTime.Puzzle.Gears
{
    public class Check : MonoBehaviour
    {
        [SerializeField] private GameObject[] gears;

        void Update()
        {
            if(gears[0].GetComponent<Spin>().targetAngle == 0 &&
               gears[1].GetComponent<Spin>().targetAngle == 0 &&
               (gears[2].GetComponent<Spin>().targetAngle == 0 || gears[2].GetComponent<Spin>().targetAngle == 180))
            {
                Debug.Log("Hello: " + gameObject.name);
            }
        }
    }
}

using UnityEngine;

namespace PuzzledTime.Puzzle.Gears
{
    public class Spin : MonoBehaviour
    {
        [SerializeField] public float targetAngle = 0;
        float time = 0.0f;
        [SerializeField] float turnSpeed = 10;
        [SerializeField] int times;
        [SerializeField] GameObject inny;
        void Update()
        {
        
            gameObject.transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, targetAngle * times), turnSpeed * Time.deltaTime);
            time -= Time.deltaTime;
        }
        void OnMouseDown()
        {
            if(time <= 0.0f)
            {
                spinnin();
                inny.GetComponent<Spin>().spinnin();
            }
       
        }
        private void spinnin()
        {
            targetAngle += 45;
            if (targetAngle == 360)
            {
                targetAngle = 0;
            }
            time = 0.1f;
        }
    }
}

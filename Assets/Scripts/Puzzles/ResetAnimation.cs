using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PuzzledTime
{
    public class ResetAnimation : MonoBehaviour
    {
        [SerializeField] private Vector2 originalPosition;

        private void OnDisable()
        {
            RectTransform position = GetComponent<RectTransform>();
            position.localPosition = originalPosition;
        }
    }
}

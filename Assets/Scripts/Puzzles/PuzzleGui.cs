﻿using PuzzledTime.Transitions;
using PuzzledTime.Utilities;
using UnityEngine;
using UnityEngine.UI;
using PuzzledTime.Movement;

namespace PuzzledTime.Puzzle
{
    public class PuzzleGui : Gui<PuzzleGui>
    {
        private GameObject m_background;
        
        protected override void Awake()
        {
            base.Awake();
            m_background = gameObject;
        }

        public void SetBackground(GameObject background)
        {
            m_background = background;
        }

        public override void Show(bool value)
        {
            if (!canBeOpened)
                return;

            isOpened = value;
            Managers.LevelManager.Instance.CanTravel = !value;
            m_background.SetActive(value);
            CharacterController2D.canMove = !value;
            Inventory.InventoryManager.Instance.canOpen = !value;


        }
    }
}

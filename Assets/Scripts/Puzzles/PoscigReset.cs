using PuzzledTime.Managers;
using PuzzledTime.Movement;
using PuzzledTime.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PuzzledTime
{
    public class PoscigReset : MonoBehaviour
    {
        private BoxCollider2D m_collider;

        private void Start()
        {
            m_collider ??= GetComponent<BoxCollider2D>();
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.gameObject.CompareTag("Player"))
                StartCoroutine(Reset());
        }

        private IEnumerator Reset()
        {
            CharacterController2D.canMove = false;
            m_collider.enabled = false;
            LevelManager.Instance.Fade.SetTrigger("Fade");

            yield return Helpers.GetWait(0.5f);
            CharacterController2D.canMove = true;
            PlayerManager.Instance.Player.transform.position = new Vector2(-4f, -5f);
            transform.position = new Vector2(-10.5f, -5f);
            m_collider.enabled = true;
            LevelManager.Instance.Fade.SetTrigger("Fade");
        }
    }
}

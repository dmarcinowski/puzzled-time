using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PuzzledTime.Puzzle
{
    public class BasementLevel6Buttons : PuzzleTrigger
    {
        [SerializeField] private int currentIndex = 0;
        [SerializeField] private string correctAnswer = "35241";
        [SerializeField] private GameObject[] buttonList;
        [SerializeField] private Canvas puzzleCanvas;
        [SerializeField] private GameObject desk;
        [SerializeField] private Sprite newDesk;
        [SerializeField] bool isPressed = false;

        private void Start()
        {
            var Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
            puzzleCanvas.worldCamera = Camera;
            puzzleCanvas.sortingLayerName = "Puzzle";
        }
        public void WriteAnswer(GameObject value)
        {
            if(!isPressed)
            {
                isPressed = !isPressed;
                StartCoroutine(PressButton(value));
            }
        }

        IEnumerator PressButton(GameObject value)
        {
            var animator = value.GetComponent<Animator>();
            animator.keepAnimatorControllerStateOnDisable = true;
            animator.SetBool("clicked", true);
            Managers.AudioManager.Instance.PlaySound("klik");
            yield return new WaitForSeconds(0.5f);
            if (!correctAnswer[currentIndex].Equals(value.name[0]))
            {
                Reset();
            }
            else
            {
                currentIndex++;
                if (currentIndex == 5)
                {
                    Win();
                }

            }
            isPressed = !isPressed;
        }
        private void Win()
        {
            Managers.AudioManager.Instance.PlaySound("correct");
            solved = true;
            Inventory.InventoryManager.Instance.RemoveItem();
        }
        private void Reset()
        {
            foreach (var buttons in buttonList)
            {
                var animator = buttons.GetComponent<Animator>();
                animator.SetBool("clicked", false);
            }
            currentIndex = 0;
        }
        protected override void OnPuzzleActive()
        {
            desk.GetComponent<SpriteRenderer>().sprite = newDesk;
            requiredItem = false;
            Inventory.InventoryManager.Instance.RemoveItem();;
        }
    }

}

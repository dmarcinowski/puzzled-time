using PuzzledTime.Dialogue;
using PuzzledTime.Interaction;
using PuzzledTime.Managers;
using PuzzledTime.Utilities;
using PuzzledTime.Movement;
using UnityEngine;
using System.Collections;

namespace PuzzledTime
{
    public class PoscigDialogue : DialogueTrigger
    {
        private bool changedTime = false;

        private bool move = false;
        private bool slowTime = false;
        private float timeScale = 1f;

        private void Update()
        {
            if (changedTime == true)
                return;

            if (Input.GetKeyDown(KeyCode.T))
            {
                changedTime = true;
                move = false;
                CharacterController2D.canMove = true;
                InteractionManager.Instance.DontDisable = false;
                LevelManager.Instance.Poscig.SetActive(false);
                InteractionGui.Instance.SetLetter(KeyCode.F);
                InteractionGui.Instance.Show(false);
                StopAllCoroutines();
                StartCoroutine(EnableTrigger());
            }

            if (timeScale < 0.05)
            {
                LevelManager.Instance.Poscig.GetComponent<Animator>().speed = 0f;
                return;
            }

            if (slowTime == true)
            {
                LevelManager.Instance.Poscig.GetComponent<Animator>().speed = timeScale * 1.1f;
            }

            if (move == true)
            {
                LevelManager.Instance.Poscig.transform.position = Vector3.MoveTowards(
                    LevelManager.Instance.Poscig.transform.position,
                    new Vector2(PlayerManager.Instance.Player.transform.position.x, LevelManager.Instance.Poscig.transform.position.y),
                    5f * Time.deltaTime * timeScale);
            }
        }

        public override void Trigger()
        {
            base.Trigger();
            if (changedTime == false)
            {
                Enabled = false;
                StartCoroutine(Poscig());
                CharacterController2D.canMove = false;
            }
        }

        private IEnumerator Poscig()
        {
            yield return Helpers.GetWait(1f);
            DialogueManager.Instance.NextSentence();
            LevelManager.Instance.Fade.SetTrigger("Fade");

            CharacterController2D.canMove = false;

            yield return Helpers.GetWait(0.5f);
            LevelManager.Instance.Poscig.transform.localScale = new Vector2(-9, 9);
            LevelManager.Instance.Poscig.transform.position = new Vector2(10.25f, -5f);
            LevelManager.Instance.Poscig.SetActive(true);
            LevelManager.Instance.Fade.SetTrigger("Fade");

            yield return Helpers.GetWait(0.5f);
            LevelManager.Instance.Poscig.GetComponent<Animator>().SetBool("run", true);
            move = true;
            slowTime = true;
            StartCoroutine(SlowTime());
            InteractionManager.Instance.DontDisable = true;
            InteractionGui.Instance.SetLetter(KeyCode.T);
            InteractionGui.Instance.Show(true);
        }

        private IEnumerator SlowTime()
        {
            while (timeScale != 0)
            {
                yield return Helpers.GetWait(0.1f);
                timeScale *= 0.95f;

                if (timeScale < 0.05)
                    break;
            }
        }

        private IEnumerator EnableTrigger()
        {
            yield return Helpers.GetWait(0.5f);
            Enabled = true;
        }
    }

}

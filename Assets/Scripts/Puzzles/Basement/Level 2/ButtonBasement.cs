using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PuzzledTime.Managers;
using PuzzledTime.Inventory.Item;
using PuzzledTime.Inventory;

namespace PuzzledTime.Puzzle
{
    public class ButtonBasement : PuzzleTrigger
    {
        [SerializeField] private string answer = "YRBYGR";
        [SerializeField] private string playerAnswer = "";
        [SerializeField] private Canvas puzzleCanvas;

        [SerializeField] private GameObject wheelParent;

        [SerializeField] private int index = 0;
        [SerializeField] private ItemObject toBeRemoved;
        [SerializeField] private GameObject objectToRemove;

        private void Start()
        {
            var Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
            puzzleCanvas.worldCamera = Camera;
            puzzleCanvas.sortingLayerName = "Puzzle";
        }

        private void Check()
        {
            if (answer == playerAnswer)
            {
                Win();
            }
            else
            {
                playerAnswer = "";
                AudioManager.Instance.PlaySound("Gong");
                index = 0;
            }
        }

        private void Win()
        {
            solved = true;

            if (objectToRemove != null) Destroy(objectToRemove);
            InventoryManager.Instance.RemoveItem(toBeRemoved.Item);

            wheelParent.transform.GetChild(0).GetComponent<Animator>().SetBool("Solved", true);
            wheelParent.transform.GetChild(1).GetComponent<Animator>().SetBool("Solved", true);
            wheelParent.transform.GetChild(2).GetComponent<Animator>().SetBool("Solved", true);
            wheelParent.transform.GetChild(3).GetComponent<Animator>().SetBool("Solved", true);
            wheelParent.transform.GetChild(4).GetComponent<Animator>().SetBool("Solved", true);
            StartCoroutine(Waiter());
        }

        public void WriteAnswer(GameObject button)
        {
            var animator = button.GetComponent<Animator>();
            animator.keepAnimatorControllerStateOnDisable = true;
            if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Buttons") && animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1.0f)
            {
                playerAnswer += button.name;
                index++;
                AudioManager.Instance.PlaySound("Button1");

                animator.SetTrigger("pressed");
                if (index == 6)
                    Check();
            }
        }

        IEnumerator Waiter()
        {
            yield return new WaitForSeconds(3);
            var desktopAnim = GetComponent<Animator>();
            desktopAnim.keepAnimatorControllerStateOnDisable = true;
            desktopAnim.SetBool("move", true);
        }

        public override void Trigger()
        {
            base.Trigger();
            playerAnswer = "";
            index = 0;
        }
    }
}

using System.Collections;
using PuzzledTime.Movement;
using PuzzledTime.Interaction;
using PuzzledTime.Managers;
using UnityEngine;


namespace PuzzledTime.Platform
{
    public class PlatformTrigger : Interactable
    {
        [SerializeField] Animator objectAnimator;
        private Animator leverAnimator;

        [SerializeField] private string m_leverName;
        
        private bool m_going;
        private bool m_finisher;

        protected override string GizmoIcon => "PuzzleTrigger.png";

        private void Start() 
        {
            leverAnimator = GetComponent<Animator>();
            leverAnimator.keepAnimatorControllerStateOnDisable = true;
            objectAnimator.keepAnimatorControllerStateOnDisable = true;
        }

        public override void Trigger()
        {
            if(m_going == false) 
            {
                leverAnimator.enabled = true;
                m_going = true;  
                Enabled = false;
                objectAnimator.SetBool("going"+m_leverName, true);
                leverAnimator.SetBool("lever", true);
                StartCoroutine(InteractionEnabler());
            }
            else if (m_going == true)
            {
                m_finisher = true;
                m_going = false;
                Enabled = false;
                objectAnimator.SetBool("going"+m_leverName, false);
                leverAnimator.SetBool("lever", false);
                StartCoroutine(InteractionEnabler());
            }
            else if(m_finisher)
            {
                m_finisher = false;
                m_going = false;
                Enabled = false;
                objectAnimator.SetBool("going"+m_leverName, false);
                StartCoroutine(InteractionEnabler());
            }
        }

        IEnumerator InteractionEnabler() 
        {
            CharacterController2D.DisableMovement(false);
            AudioManager.Instance.PlaySound("Lever");
            yield return new WaitForSeconds(2);
            CharacterController2D.DisableMovement(true);
            Enabled = true;
        }
    }
}

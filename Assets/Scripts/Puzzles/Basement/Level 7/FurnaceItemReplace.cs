using System.Collections;
using UnityEngine;
using PuzzledTime.Managers;
using PuzzledTime.Inventory;
using PuzzledTime.Inventory.Item;
using PuzzledTime.Movement;
using PuzzledTime.Interaction;
using PuzzledTime.Dialogue;

namespace PuzzledTime.Puzzle
{
    public class FurnaceItemReplace : PuzzleTrigger
    {
        [Header("Item To Replace")]
        [SerializeField] private ItemObject[] toBeAdded;
        [SerializeField] private ItemObject sczypce; // tak nie pamietam jak sa po angielsku szczypce XD
        [SerializeField] private GameObject smoke;

        private Animator m_animator;

        private void Awake()
        {
            m_animator = smoke.GetComponent<Animator>();
        }

        protected override string GizmoIcon => "PuzzleTrigger.png";

        public override void Trigger()
        {
            if(requiredItem && CheckIfHasItem() || !requiredItem)
            {
                CharacterController2D.canMove = false;
                InteractionGui.Instance.Disable(true);
                var currentItem = InventoryManager.Instance.GetCurrentitem();
                int index = -1;
                for (var i = 0; i < listOfItem.Length; i++)
                {
                    if (listOfItem[i].Item.Id == currentItem.Id)
                    {
                        index = i;
                        break;
                    }
                }
                Managers.LevelManager.Instance.CanTravel = false;
                StartCoroutine(Wait(index));
            }
            else
            {
            
                    InteractionGui.Instance.Disable(true);
                InventoryManager.Instance.canOpen = false;
                DialogueManager.Instance.StartDialogue(dialogueNotHaveItem.Dialogue);

            }
        }

        private IEnumerator Wait(int index)
        {
            m_animator.SetTrigger("smoke");
            yield return new WaitForSeconds(2f);
            InventoryManager.Instance.ReplaceItem(listOfItem[index].Item, toBeAdded[index].Item);
            InventoryManager.Instance.AddItem(sczypce.Item);
            CharacterController2D.canMove = true;
            InventoryManager.Instance.canOpen = true;
            Managers.LevelManager.Instance.CanTravel = true;
            Enabled = true;
        }
    }
}

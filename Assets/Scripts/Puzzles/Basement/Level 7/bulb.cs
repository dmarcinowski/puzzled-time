using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PuzzledTime.Puzzle
{
    public class bulb : PuzzleTrigger
    {
        [SerializeField] Sprite fixedSprite;
        [SerializeField] GameObject newLight;

        public override void Trigger()
        {
            if (requiredItem && CheckIfHasItem() || !requiredItem)
            {
                GetComponent<SpriteRenderer>().sprite = fixedSprite;
                newLight.SetActive(true);
                solved = true;

            }
            else
            {
                Dialogue.DialogueManager.Instance.StartDialogue(dialogueNotHaveItem.Dialogue);
            }
        }
    }
}

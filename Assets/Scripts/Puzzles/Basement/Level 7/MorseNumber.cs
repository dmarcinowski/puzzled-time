using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PuzzledTime.Puzzle
{
    public class MorseNumber : MonoBehaviour
    {
        private int numberName;
        private int index = 0;
        [SerializeField] private Sprite[] numberSprites;

        void Start()
        {
            numberName = 0;
            gameObject.name = numberName.ToString();
        }

        public void NumberDown()
        {
            if (index != 0)
                --index;
            Check();
        }

        public void NumberUp()
        {
            if (index != 9)
                ++index;
            Check();
        }

        private void Check()
        {
            numberName = index;
            gameObject.name = numberName.ToString();
            GetComponent<Image>().sprite = numberSprites[index];
        }
    }
}

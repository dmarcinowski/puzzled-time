using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PuzzledTime.Inventory;
using PuzzledTime.Inventory.Item;
using PuzzledTime.Puzzle;

namespace PuzzledTime.Puzzle
{
    public class FurnanceOutput : ItemTrigger
    {
        [SerializeField] FurnaceItem furnance;
        [SerializeField] Dialogue.DialogueObject dialouge;
        public override void Trigger()
        {
            if(InventoryManager.Instance.GetCurrentId() == 11)
            {
                InventoryManager.Instance.RemoveItem();
                InventoryManager.Instance.AddItem(item.Item);
                furnance.Enabled = true;
                furnance.gameObject.GetComponent<BoxCollider2D>().enabled = true;
                gameObject.GetComponent<SpriteRenderer>().sprite = null;
                Enabled = false;
                gameObject.GetComponent<BoxCollider2D>().enabled = false;
            }
            else
            {
                Dialogue.DialogueManager.Instance.StartDialogue(dialouge.Dialogue);
            }
        }
        public void SetItem(ItemObject newItem)
        {
            item = newItem;
            Enabled = true;
            gameObject.GetComponent<BoxCollider2D>().enabled = true;
            gameObject.GetComponent<SpriteRenderer>().sprite = newItem.ItemSprite;
        }
    }
}

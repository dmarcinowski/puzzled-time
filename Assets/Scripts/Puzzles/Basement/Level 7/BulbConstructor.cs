using System.Collections;
using UnityEngine;
using PuzzledTime.Managers;
using PuzzledTime.Inventory;
using PuzzledTime.Inventory.Item;
using PuzzledTime.Movement;

namespace PuzzledTime.Puzzle
{
    public class BulbConstructor : PuzzleTrigger
    {
        [Header("To Be Removed")]
        [SerializeField] private ItemObject toBeAdded;

        protected override string GizmoIcon => "PuzzleTrigger.png";

        public override void Trigger()
        {
            if(requiredItem && CheckIfHasItem() || !requiredItem)
            {
                Enabled = false;
                foreach(var item in listOfItem)
                {
                    InventoryManager.Instance.RemoveItem(item.Item);
                }
                InventoryManager.Instance.AddItem(toBeAdded.Item);
                requiredItem = false;
                InventoryManager.Instance.canOpen = false;
                StartCoroutine(Wait());
            }
            else
            {

                Dialogue.DialogueManager.Instance.StartDialogue(dialogueNotHaveItem.Dialogue);
            }
        }

        private IEnumerator Wait()
        {
            CharacterController2D.canMove = false;
            yield return new WaitForSeconds(1);
            Enabled = true;
            gameObject.SetActive(false);
            CharacterController2D.canMove = true;
            InventoryManager.Instance.canOpen = true;
        }
    }
}

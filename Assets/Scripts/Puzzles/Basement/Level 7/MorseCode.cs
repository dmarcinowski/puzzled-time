using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PuzzledTime.Managers;
using PuzzledTime.Inventory.Item;
using PuzzledTime.Inventory;

namespace PuzzledTime.Puzzle
{
    public class MorseCode : PuzzleTrigger
    {
        [SerializeField] private string answer = "3529";
        [SerializeField] private string playerAnswer = "";
        [SerializeField] private Canvas puzzleCanvas;

        [SerializeField] private GameObject[] numberObjects;
        [SerializeField] private ItemObject toBeAdded;

        private Animator chestAnim;

        [SerializeField] private int numberCount = 0;

        private void Start()
        {
            var Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
            puzzleCanvas.worldCamera = Camera;
            puzzleCanvas.sortingLayerName = "Puzzle";
            chestAnim = gameObject.GetComponent<Animator>();
        }

        private void Check()
        {
            if (answer == playerAnswer)
            {
                solved = true;
                chestAnim.enabled = true;
                InventoryManager.Instance.AddItem(toBeAdded.Item);
            }
            else
            {
                playerAnswer = "";
                numberCount = 0;
            }
        }

        public void CheckAnswer() 
        {
            if(numberCount != 4)
            {
                for(var i = 0; i < numberObjects.Length; i++)
                {
                    playerAnswer += numberObjects[i].name;
                    numberCount++;
                    if(numberCount == 4)
                    {
                        Check();
                    }
                }
            }
        }

        public override void Trigger()
        {
            base.Trigger();
        }
    }
}

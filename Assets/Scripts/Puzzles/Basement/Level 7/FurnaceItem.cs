using System.Collections;
using UnityEngine;
using PuzzledTime.Managers;
using PuzzledTime.Inventory;
using PuzzledTime.Inventory.Item;
using PuzzledTime.Movement;
using PuzzledTime.Dialogue;
using PuzzledTime.Interaction;

namespace PuzzledTime.Puzzle
{
    public class FurnaceItem : PuzzleTrigger
    {
        [Header("To Be Removed")]
        [SerializeField] private ItemObject[] toBeConverted;
        [SerializeField] private ItemObject[] toBeCreated;
        [SerializeField] private GameObject Output;
        [SerializeField] private bool hasWood = false;
        [SerializeField] private bool hasFire = false;

        [SerializeField] private DialogueObject secondDialogue;

        private Animator m_animator;

        protected override string GizmoIcon => "PuzzleTrigger.png";

        private void Awake()
        {
            m_animator = GetComponent<Animator>();
            m_animator.keepAnimatorControllerStateOnDisable = true;
        }
        public override void Trigger()
        {
            if (requiredItem && CheckIfHasItem() || !requiredItem)
            {
                InteractionGui.Instance.Disable(true);
                var item_id = Inventory.InventoryManager.Instance.GetCurrentId();
                if (!hasWood)
                {
                    if (item_id == 8)
                    {
                        hasWood = true;
                        transform.GetChild(0).gameObject.SetActive(true);
                        Inventory.InventoryManager.Instance.RemoveItem();
                        return;
                    }
                }
                if (!hasFire && hasWood)
                {
                    if (item_id == 12)
                    {
                        hasFire = true;
                        m_animator.SetTrigger("fire");
                        Inventory.InventoryManager.Instance.RemoveItem();
                        requiredItem = false;
                        return;
                    }
                }
                if (hasFire)
                {
                    for (var i = 0; i < toBeConverted.Length; i++)
                    {
                        if (toBeConverted[i].Item.Id == item_id)
                        {
                            CharacterController2D.canMove = false;
                            InventoryManager.Instance.canOpen = false;
                            StartCoroutine(Wait(i));
                            break;
                        }
                    }
                }
                else
                {

                }
            }
            else
            {
                    if (!hasWood)
                    {
                        DialogueManager.Instance.StartDialogue(dialogueNotHaveItem.Dialogue);
                    }
                    else if (!hasFire)
                    { 
                        DialogueManager.Instance.StartDialogue(secondDialogue.Dialogue);
                    }
                
            }
        }

        private IEnumerator Wait(int index)
        {
            Enabled = false;
            gameObject.GetComponent<BoxCollider2D>().enabled = false; 
            yield return new WaitForSeconds(1f);
            InventoryManager.Instance.RemoveItem();
            Output.GetComponent<FurnanceOutput>().SetItem(toBeCreated[index]);
            CharacterController2D.canMove = true;
            InventoryManager.Instance.canOpen = true;
        }


    }
}

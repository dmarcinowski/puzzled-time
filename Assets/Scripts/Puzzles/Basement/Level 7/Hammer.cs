using System.Collections;
using UnityEngine;
using PuzzledTime.Inventory;
using PuzzledTime.Inventory.Item;
using PuzzledTime.Movement;

namespace PuzzledTime.Puzzle
{
    public class Hammer : PuzzleTrigger
    {
        [SerializeField] private ItemObject[] toBeAdded;
        [SerializeField] private GameObject hammer;

        Animator m_animator;

        protected override string GizmoIcon => "PuzzleTrigger.png";

        private void Awake()
        {
            m_animator = hammer.GetComponent<Animator>();
        }
        public override void Trigger()
        {
           
            if(requiredItem && CheckIfHasItem() || !requiredItem)
            {
                Enabled = false;
                OnPuzzleActive();
                CharacterController2D.canMove = false;
                var currentItem = InventoryManager.Instance.GetCurrentitem();
                int index =-1;
                for(var i =0; i < listOfItem.Length; i++)
                {
                    if (listOfItem[i].Item.Id == currentItem.Id)
                    {
                        index = i;
                        break;
                    }
                }
                Managers.LevelManager.Instance.CanTravel = false;
                StartCoroutine(Wait(index));
            }
            else
            {
                Dialogue.DialogueManager.Instance.StartDialogue(dialogueNotHaveItem.Dialogue);
            }
        }

        private IEnumerator Wait(int index) 
        {
            Managers.AudioManager.Instance.PlaySound("anvil");
            m_animator.SetTrigger("work");
            yield return new WaitForSeconds(2f);
            CharacterController2D.canMove = true;
            Managers.LevelManager.Instance.CanTravel = true;
            InventoryManager.Instance.ReplaceItem(listOfItem[index].Item, toBeAdded[index].Item);
            Enabled = true;
        }
    }
}

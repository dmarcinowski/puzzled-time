using System.Collections;
using UnityEngine;
using PuzzledTime.Managers;
using PuzzledTime.Inventory;
using PuzzledTime.Inventory.Item;
using PuzzledTime.Movement;
using PuzzledTime.Dialogue;
using PuzzledTime.Interaction;

namespace PuzzledTime.Puzzle
{
    public class Radio : PuzzleTrigger
    {
        [SerializeField] private ItemObject toBeRemoved;
        protected override string GizmoIcon => "PuzzleTrigger.png";

        public override void Trigger()
        {
            if(requiredItem && CheckIfHasItem() || !requiredItem)
            {
                Enabled = false;
                InventoryManager.Instance.RemoveItem(toBeRemoved.Item);
                requiredItem = false;
                Managers.LevelManager.Instance.CanTravel = false;
                StartCoroutine(MorseCode());
            }
            else
            {

                InteractionGui.Instance.Disable(true);
                InventoryManager.Instance.canOpen = true;
                DialogueManager.Instance.StartDialogue(dialogueNotHaveItem.Dialogue);

            }
        }

        private IEnumerator MorseCode()
        {
            CharacterController2D.canMove = false;
            AudioManager.Instance.PlaySound("3");
            yield return new WaitForSeconds(2);
            AudioManager.Instance.PlaySound("5");
            yield return new WaitForSeconds(2);
            AudioManager.Instance.PlaySound("2");
            yield return new WaitForSeconds(2);
            AudioManager.Instance.PlaySound("9");
            yield return new WaitForSeconds(2);
            Enabled = true;
            CharacterController2D.canMove = true;
            Managers.LevelManager.Instance.CanTravel = true;
        }
    }
}

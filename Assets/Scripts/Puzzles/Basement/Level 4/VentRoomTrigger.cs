using UnityEngine;
using PuzzledTime.Level;
using PuzzledTime.Managers;

namespace PuzzledTime
{
    public class VentRoomTrigger : RoomTrigger
    {
        protected override string GizmoIcon => "DoorTrigger.png";

        public override void Trigger()
        {
            base.Trigger();
            LevelManager.Instance.Poscig.GetComponent<Animator>().SetBool("run", false);
            LevelManager.Instance.Poscig.SetActive(false);
        }
    }
}

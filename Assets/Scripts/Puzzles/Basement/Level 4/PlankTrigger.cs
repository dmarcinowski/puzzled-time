using PuzzledTime.Interaction;
using PuzzledTime.Movement;
using PuzzledTime.Managers;
using PuzzledTime.Utilities;
using PuzzledTime.Puzzle;
using System.Collections;
using UnityEngine;

namespace PuzzledTime
{
    public class PlankTrigger : Interactable
    {
        [SerializeField] private Animator plankAnimator;
        [SerializeField] private VentTrigger ventTrigger;

        protected override string GizmoIcon => "DoorTrigger.png";

        private bool move = false;
        private void FixedUpdate()
        {
            if (ventTrigger.PoscigMove == true && move == true)
            {
                LevelManager.Instance.Poscig.GetComponent<Animator>().SetBool("run", true);
                LevelManager.Instance.Poscig.transform.position = Vector3.MoveTowards(
                    LevelManager.Instance.Poscig.transform.position,
                    new Vector2(PlayerManager.Instance.Player.transform.position.x, LevelManager.Instance.Poscig.transform.position.y),
                    5f * Time.fixedDeltaTime);
            }
        }

        public override void Trigger()
        {
            plankAnimator.SetBool("Fall", true);
            Enabled = false;

            // Poscig sequence
            CharacterController2D.canMove = false;
            StartCoroutine(SpawnPoscig(1f));
        }

        IEnumerator SpawnPoscig(float time)
        {
            // Fade them in
            yield return Helpers.GetWait(time);
            LevelManager.Instance.Fade.SetTrigger("Fade");

            yield return Helpers.GetWait(0.5f);
            LevelManager.Instance.Poscig.SetActive(true);
            LevelManager.Instance.Poscig.transform.position = new Vector2(-10.5f, 2f);

            yield return Helpers.GetWait(0.5f);
            LevelManager.Instance.Fade.SetTrigger("Fade");

            // Climb down
            yield return Helpers.GetWait(1f);
            LevelManager.Instance.Fade.SetTrigger("Fade");

            yield return Helpers.GetWait(0.5f);
            LevelManager.Instance.Poscig.SetActive(true);
            LevelManager.Instance.Poscig.transform.position = new Vector2(-10.5f, -5f);

            yield return Helpers.GetWait(0.5f);
            LevelManager.Instance.Fade.SetTrigger("Fade");

            // Running
            yield return Helpers.GetWait(0.5f);
            CharacterController2D.canMove = true;
            move = true;
        }
    }
}

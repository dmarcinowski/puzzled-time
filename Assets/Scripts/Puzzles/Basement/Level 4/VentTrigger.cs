using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using PuzzledTime.Inventory;
using PuzzledTime.Dialogue;
using PuzzledTime.Managers;

namespace PuzzledTime.Puzzle
{
    public class VentTrigger : PuzzleTrigger
    {
        [SerializeField] private Canvas puzzleCanvas;
        [SerializeField] private GameObject door;
        private int screwCount;
        private int allScrews = 4;

        protected override string GizmoIcon => "DoorTrigger.png";

        public bool PoscigMove = true;

        public override void Trigger()
        {
            base.Trigger();
            PoscigMove = false;
        }

        private void Start()
        {
            var Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
            puzzleCanvas.worldCamera = Camera;
            puzzleCanvas.sortingLayerName = "Puzzle";
        }

        protected override void Update()
        {
            base.Update();
            if (Input.GetKeyDown(KeyCode.Escape))
                PoscigMove = true;
        }

        public void Unscrew(GameObject screw)
        {
            var animator = screw.GetComponent<Animator>();
            animator.keepAnimatorControllerStateOnDisable = true;
            screwCount++;
            animator.SetBool("unscrew", true);
            if (screwCount == allScrews)
            {
                Win();
            }
            StartCoroutine(Disabler(animator.gameObject));
        }

        private IEnumerator Disabler(GameObject screw)
        {
            var button = screw.GetComponent<Button>();
            button.interactable = false;
            yield return new WaitForSeconds(2f);
            screw.SetActive(false);
        }

        private void Win()
        {
            solved = true;
            door.SetActive(true);
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            StartCoroutine(EnablePoscig());
        }

        private IEnumerator EnablePoscig(float time = 3f)
        {
            yield return new WaitForSeconds(time);
            PoscigMove = true;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PuzzledTime.Puzzle
{
    public class BasementPipesPuzzle : PuzzleTrigger
    {
        [SerializeField] BasementPipeButton[] Pipes;
        [SerializeField] List<int> usedNumbers = new List<int>();

        [SerializeField] private Animator steamAnimator;

        [SerializeField] private Canvas puzzleCanvas;

        private readonly int m_steamAnimation = Animator.StringToHash("Solved");

        [SerializeField] private GameObject resetObject;

        int _previousNumber = 0;

        private void Start() 
        {
            var Camera = GameObject.Find("Main Camera").GetComponent<Camera>();
            puzzleCanvas.worldCamera = Camera;
            puzzleCanvas.sortingLayerName = "Puzzle";
        }

        private void Awake()
        {
            int number = Random.Range(0, 5);
            int number2 = number;
            while (number2 == number)
            {
                number2 = Random.Range(0, 5);
            }
            for (var i = 0; i < Pipes.Length; i++)
            {
                if (number != i && number2 != i)
                    GenerateNumber(Pipes[i], i);
            }

        }
        protected override void Update()
        {
            base.Update();
            if(IsFinished())
            {
                solved = true;
                steamAnimator.SetBool(m_steamAnimation, true);
            }
        }

        private void GenerateNumber(BasementPipeButton pipe, int index)
        {
            pipe.multiAnswer = true;
            bool isUniqe = false;
            int number = -1;

            while (!isUniqe)
            {
                number = Random.Range(0, 5);
                if(index != number && _previousNumber != number)
                {
                    isUniqe = true;
                    foreach (var elements in usedNumbers)
                    {
                        if (elements == number)
                        {
                            isUniqe = false;
                            break;
                        }

                    }
                }
            }
            pipe.addNumber(number);
            _previousNumber++;
            usedNumbers.Add(number);
        }
       
        bool IsFinished()
        {
            foreach (var elements in Pipes)
            {
                if(!elements.active)
                {
                    return false;

                }
            }
                return true;
        }

        public void ChangeValue(int index)
        {
            var pipe = Pipes[index].GetComponent<BasementPipeButton>();
            pipe.ChangeValue();
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PuzzledTime
{
    public class ObjectDisabler : MonoBehaviour
    {
        [SerializeField] private GameObject disableObject;
        public void DisableSteamMovement()
        {
            disableObject.SetActive(false);
        }
    }
}

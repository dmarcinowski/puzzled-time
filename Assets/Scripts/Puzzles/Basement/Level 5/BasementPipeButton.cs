using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PuzzledTime.Puzzle
{
    public class BasementPipeButton : MonoBehaviour
    {
        public bool active = false;
        public bool multiAnswer = false;

        [SerializeField] private GameObject parent;
        [SerializeField] private int secondNumber;
        [SerializeField] private Sprite buttonOn;
        [SerializeField] private Sprite buttonOff;

        public void addNumber(int number)
        {
            secondNumber = number;
        }
        public void ChangeValue()
        {
            active = !active;
            if (active)
                transform.GetChild(0).GetComponent<Image>().sprite = buttonOff;
            else
                transform.GetChild(0).GetComponent<Image>().sprite = buttonOn;
        }

        public void Pressed()
        {
            Managers.AudioManager.Instance.PlaySound("valve");
            ChangeValue();
            if(multiAnswer)
                parent.GetComponent<BasementPipesPuzzle>().ChangeValue(secondNumber);
        }
    }
}

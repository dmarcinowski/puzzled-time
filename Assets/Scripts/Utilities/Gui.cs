﻿using UnityEngine;
using PuzzledTime.Movement;

namespace PuzzledTime.Utilities
{
    [RequireComponent(typeof(Animator))]
    public abstract class Gui<T> : Singleton<T> where T : MonoBehaviour
    {
        [SerializeField, DisableInPlayMode] protected bool isOpened;
        [SerializeField] protected bool canBeOpened = true;
        [SerializeField] protected bool canBeClosed = true;

        public bool IsOpened => isOpened;
        public bool CanBeOpened { get => canBeOpened; set => canBeOpened = value; }
        public bool CanBeClosed { get => canBeClosed; set => canBeClosed = value; }

        protected Animator Animator;
        private readonly int m_showAnimation = Animator.StringToHash("Show");

        protected override void Awake()
        {
            base.Awake();
            Animator = gameObject.GetComponent<Animator>();
        }

        protected virtual void Update()
        {
            if (isOpened && canBeClosed)
            {
                Time.timeScale = 0f;

                if (Input.GetKeyDown(KeyCode.Escape))
                    Show(false);
            }
    
            if (!isOpened && canBeClosed)
                Time.timeScale = 1f;
        }

        public virtual void Show(bool value)
        { 
            if (!canBeOpened)
                return;
    
            isOpened = value;
            Animator.SetBool(m_showAnimation, value);
        }
    }
}

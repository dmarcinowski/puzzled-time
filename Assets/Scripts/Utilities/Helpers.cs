﻿using System.Collections.Generic;
using UnityEngine;

namespace PuzzledTime.Utilities
{
    public static class Helpers
    {
        // Cache Main camera just once to save on Find() calls.
        private static Camera camera;
        public static Camera Camera => camera ??= Camera.main;
        
        // Cache WaitForSeconds so it does not allocate on every call.
        private static readonly Dictionary<float, WaitForSeconds> WaitForSecondsMap = new Dictionary<float, WaitForSeconds>();
        public static WaitForSeconds GetWait(float time)
        {
            if (WaitForSecondsMap.TryGetValue(time, out var wait))
                return wait;

            WaitForSecondsMap[time] = new WaitForSeconds(time);
            return WaitForSecondsMap[time];
        }
        
        // Destroy all children of transform.
        public static void DestroyChildren(this Transform t)
        {
            foreach (Transform child in t)
                Object.Destroy(child.gameObject);
        }
    }
}

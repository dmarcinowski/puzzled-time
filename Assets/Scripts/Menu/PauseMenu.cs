using UnityEngine;
using UnityEngine.SceneManagement;

namespace PuzzledTime.Menu
{
    public class PauseMenu : MonoBehaviour 
    {
        [SerializeField] private GameObject pauseMenuUI;

        private bool m_paused;
        
        private void Update()
        {
            if (!Input.GetKeyDown(KeyCode.Escape))
                return;
            
            if (m_paused)
                Resume();
            else
                Pause();
        }

        private void Resume()
        {
            pauseMenuUI.SetActive(false);
            Time.timeScale = 1f;
            m_paused = false;
        }

        private void Pause()
        {
            pauseMenuUI.SetActive(true);
            Time.timeScale = 0f;
            m_paused = true;
        }

        public void ActionLoadMenu()
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene("Menu");
        }
    }
}



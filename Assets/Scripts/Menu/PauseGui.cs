using UnityEngine;
using UnityEngine.SceneManagement;
using PuzzledTime.Utilities;
using PuzzledTime.Puzzle;
using PuzzledTime.Transitions;
using PuzzledTime.Movement;
using PuzzledTime.Managers;
using PuzzledTime.Inventory;
using PuzzledTime.Dialogue;

namespace PuzzledTime
{
    public class PauseGui : Gui<PauseGui>
    {
        protected override void Update()
        {
            if (PuzzleGui.Instance.IsOpened || InventoryGui.Instance.IsOpened)
                return;

            if (Input.GetKeyDown(KeyCode.Escape) && PlayerManager.Instance.Controller2D.Grounded && !DialogueGui.Instance.IsOpened) 
            {
                Show(!isOpened);
            }
        }

        public void ActionLoadMenu()
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene("Menu");
            GameManager.Instance.GameState = GameStates.MainMenu;
        }

        public void ActionClose()
        {
            Show(false);
        }

        public override void Show(bool value)
        {
            base.Show(value);
            TransitionGui.Instance.Transition(TransitionType.Gui, value);
            CharacterController2D.canMove = !value;
        }
    }
}

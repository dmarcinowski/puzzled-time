using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace PuzzledTime.Menu
{
    public class SettingsMenu : MonoBehaviour
    {
        [SerializeField] private Toggle fullscreenTog, vsyncTog;

        [SerializeField] private List<ResItem> resolutions = new List<ResItem>();
        [FormerlySerializedAs("qualitys")] [SerializeField] private List<string> qualities = new List<string>();

        [SerializeField] private Text resolutionLabel, qualityLabel;

        private int m_currentResolutionIndex;
        private int m_currentQualityIndex;

        [SerializeField] private AudioMixer theMixer;

        [SerializeField] private Text masterLabel, musicLabel, sfxLabel;

        [SerializeField] private Slider masterSlider, musicSlider, sfxSlider;

        private void Start()
        {
            fullscreenTog.isOn = Screen.fullScreen;

            if (QualitySettings.vSyncCount == 0)
            {
                vsyncTog.isOn = false;
            }
            else 
            {
                vsyncTog.isOn = true;
            }

            var foundRes = false;
            for (var i = 0; i < resolutions.Count; i++)
            {
                if (Screen.width == resolutions[i].horizontal && Screen.height == resolutions[i].vertical)
                {
                    foundRes = true;
                    m_currentResolutionIndex = i;
                    UpdateResolutionLabel();
                }
            }

            if (!foundRes)
            {
                var newRes = new ResItem {
                    horizontal = Screen.width,
                    vertical = Screen.height
                };

                resolutions.Add(newRes);
                m_currentResolutionIndex = resolutions.Count - 1;

                UpdateResolutionLabel();
            }

            theMixer.GetFloat("MasterVol", out var vol);
            masterSlider.value = vol;
            theMixer.GetFloat("MusicVol", out vol);
            musicSlider.value = vol;
            theMixer.GetFloat("SfxVol", out vol);
            sfxSlider.value = vol;      

            masterLabel.text = Mathf.RoundToInt(masterSlider.value + 80).ToString();
            musicLabel.text = Mathf.RoundToInt(musicSlider.value + 80).ToString();
            sfxLabel.text = Mathf.RoundToInt(sfxSlider.value + 80).ToString();
        }

        public void ApplyGraphics()
        {
            Screen.SetResolution(resolutions[m_currentResolutionIndex].horizontal, resolutions[m_currentResolutionIndex].vertical, fullscreenTog.isOn);

            QualitySettings.SetQualityLevel(m_currentQualityIndex);

            if (vsyncTog.isOn)
            {
                QualitySettings.vSyncCount = 1;
            }
            else
            {
                QualitySettings.vSyncCount = 0;
            }
        }

        public void ResolutionPrevious()
        {
            m_currentResolutionIndex--;
            if(m_currentResolutionIndex < 0)
            {
                m_currentResolutionIndex = 0;
            }
            UpdateResolutionLabel();
        }

        public void ResolutionNext()
        {
            m_currentResolutionIndex++;
            if(m_currentResolutionIndex > resolutions.Count - 1)
            {
                m_currentResolutionIndex = resolutions.Count - 1;
            }
            UpdateResolutionLabel();
        }

        private void UpdateResolutionLabel()
        {
            resolutionLabel.text = resolutions[m_currentResolutionIndex].horizontal.ToString() + " x " + resolutions[m_currentResolutionIndex].vertical.ToString();
        }

        public void QualityPrevious()
        {
            m_currentQualityIndex--;
            if(m_currentQualityIndex < 0)
            {
                m_currentQualityIndex = 0;
            }
            UpdateQualityLabel();
        }

        public void QualityNext()
        {
            m_currentQualityIndex++;
            if(m_currentQualityIndex > qualities.Count - 1)
            {
                m_currentQualityIndex = qualities.Count - 1;
            }
            UpdateQualityLabel();
        }

        private void UpdateQualityLabel()
        {
            qualityLabel.text = qualities[m_currentQualityIndex];
        }

        public void SetMasterVolume()
        {
            masterLabel.text = Mathf.RoundToInt(masterSlider.value + 80).ToString();

            theMixer.SetFloat("MasterVol", masterSlider.value);

            PlayerPrefs.SetFloat("MasterVol", masterSlider.value);
        }

        public void SetMusicVolume()
        {
            musicLabel.text = Mathf.RoundToInt(musicSlider.value + 80).ToString();

            theMixer.SetFloat("MusicVol", musicSlider.value);

            PlayerPrefs.SetFloat("MusicVol", musicSlider.value);
        }

        public void SetSfxVolume()
        {
            sfxLabel.text = Mathf.RoundToInt(sfxSlider.value + 80).ToString();

            theMixer.SetFloat("SfxVol", sfxSlider.value);

            PlayerPrefs.SetFloat("SfxVol", sfxSlider.value);
        }
    }

    [System.Serializable]
    public class ResItem 
    {
        public int horizontal, vertical;
    }
}
using PuzzledTime.Managers;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PuzzledTime.Menu
{
    public class GameMenu : MonoBehaviour
    {
        private int m_sceneToLoad;

        public void NewGame()
        {
            m_sceneToLoad = 1;
            SceneManager.LoadScene(m_sceneToLoad);
            GameManager.Instance.GameState = GameStates.InGame;
        }
    }
}

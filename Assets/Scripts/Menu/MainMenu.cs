using UnityEngine;

namespace PuzzledTime.Menu
{
    public class MainMenu : MonoBehaviour
    {
        private void Start() 
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        public void QuitGame()
        {
            Debug.Log("Quitting game...");
            Application.Quit();
        }
    }
}

﻿using PuzzledTime.Player;
using PuzzledTime.Utilities;
using PuzzledTime.Movement;
using UnityEngine;

namespace PuzzledTime.Managers
{
    public class PlayerManager : Singleton<PlayerManager>
    {
        [SerializeField] private GameObject playerPrefab;

        public GameObject Player { get; private set; }
        public StartPosition StartPosition { get; set; }

        public PlayerInteraction Interaction => Player.GetComponent<PlayerInteraction>();
        public PlayerMovement Movement => Player.GetComponent<PlayerMovement>();
        public CharacterController2D Controller2D => Player.GetComponent<CharacterController2D>();

        public void SpawnPlayer()
        {
            Player ??= Instantiate(playerPrefab);
            // StartPosition.Trigger();
        }
    }
}

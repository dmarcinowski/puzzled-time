using PuzzledTime.Utilities;
using UnityEngine;

namespace PuzzledTime.Managers
{
    public enum GameStates
    {
        Init,
        MainMenu,
        InGame,
        InPuzzle,
        Paused,
    }

    public class GameManager : SingletonPersist<GameManager>
    {
        private readonly SaveManager m_saveManager = new SaveManager();

        public GameStates GameState { get; set; } = GameStates.MainMenu;
        private GameStates prevGameState = GameStates.Init;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Z))
                m_saveManager.Save();
            if (Input.GetKeyDown(KeyCode.L))
                m_saveManager.Load(@"C:\Users\Damian\Saved Games\Puzzled Time\20_13_28_3_2022.pt");

            if (Input.GetKeyDown(KeyCode.F11))
                Screen.fullScreen = !Screen.fullScreen;

            if (GameState != prevGameState)
                GameStateChanged();
            prevGameState = GameState;
        }

        private void GameStateChanged()
        {
            switch (GameState)
            {
                case GameStates.MainMenu:
                    AudioManager.Instance.PlayMusic("menu_1");
                    break;
                case GameStates.InGame:
                    AudioManager.Instance.ToggleMusic(true);
                    AudioManager.Instance.PlayMusic("ambient_1");
                    break;
                case GameStates.Paused:
                    AudioManager.Instance.ToggleMusic(false);
                    break;
            }
        }
    }
}

using PuzzledTime.Utilities;
using UnityEngine;
using UnityEngine.Audio;
using System.Linq;
using System.Collections.Generic;

namespace PuzzledTime.Managers
{
    public class AudioManager : SingletonPersist<AudioManager>
    {
        [SerializeField] private AudioMixer theMixer;

        [Header("Sound Effects")]
        [SerializeField] private AudioSource sfx;
        [SerializeField, NewLabel("Clip", "Element"), ReorderableList]
        private List<AudioClip> sfxClips;

        [Header("Music")]
        [SerializeField] private AudioSource music;
        [SerializeField, NewLabel("Clip", "Element"), ReorderableList]
        private List<AudioClip> musicClips;

        private void Start()
        {
            if (PlayerPrefs.HasKey("MasterVol"))
                theMixer.SetFloat("MasterVol", PlayerPrefs.GetFloat("MasterVol"));

            if (PlayerPrefs.HasKey("MusicVol"))
                theMixer.SetFloat("MusicVol", PlayerPrefs.GetFloat("MusicVol"));

            if (PlayerPrefs.HasKey("SfxVol"))
                theMixer.SetFloat("SfxVol", PlayerPrefs.GetFloat("SfxVol"));
        }

        public void PlaySound(string name)
        {
            if (name == string.Empty)
                return;
            sfx.PlayOneShot(sfxClips.SingleOrDefault(a => a.name.Equals(name)));
        }

        public void PlayMusic(string name)
        {
            music.clip = musicClips.SingleOrDefault(a => a.name.Equals(name));
            music.Play();
        }

        public void ToggleMusic(bool playMusic)
        {
            if (playMusic)
                music.Pause();
            else
                music.Play();
        }
    }
}

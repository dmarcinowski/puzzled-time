﻿using PuzzledTime.Interaction;
using PuzzledTime.Inventory;
using PuzzledTime.Puzzle;
using PuzzledTime.Utilities;
using UnityEngine;

namespace PuzzledTime.Managers
{
    public class InteractionManager : Singleton<InteractionManager>
    {
        [SerializeField, SearchableEnum] private KeyCode interactionKey;

        public KeyCode InteractionKey => interactionKey;
        public Transform PlayerPoint { get; set; }
        public bool DontDisable = false;

        private Interactable m_interactedObject;

        private void Start()
        {
            InteractionGui.Instance.SetLetter(interactionKey);
        }

        private void Update()
        {
            if (DontDisable == true)
            {
                InteractionGui.Instance.UpdatePosition(PlayerPoint);
                return;
            }

            if (m_interactedObject == null)
                return;

            if (PuzzleGui.Instance.IsOpened || InventoryGui.Instance.IsOpened || !m_interactedObject.Enabled)
                return;
           

            InteractionGui.Instance.UpdatePosition(PlayerPoint);

            if (m_interactedObject.Automatic)
            {
                m_interactedObject.Enabled = false;
                m_interactedObject.Trigger();
                return;
            }
            
            if (Input.GetKeyDown(interactionKey) && PlayerManager.Instance.Controller2D.Grounded && !PauseGui.Instance.IsOpened)
                m_interactedObject.Trigger();
        }

        public void OnInteractionEnter(Collider2D col)
        {
            if (DontDisable == true)
                return;

            if (!col.TryGetComponent<Interactable>(out var interactable))
                return;
            
            m_interactedObject = interactable;
            InteractionGui.Instance.Show(m_interactedObject.Enabled && !m_interactedObject.Automatic && PlayerManager.Instance.Controller2D.Grounded);
        }
        
        public void OnInteractionExit()
        {
            if (DontDisable == true)
                return;

            m_interactedObject = null;
            InteractionGui.Instance.Show(false);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PuzzledTime.Level;
using PuzzledTime.Utilities;
using PuzzledTime.Movement;
using UnityEngine;

namespace PuzzledTime.Managers
{
    public class LevelManager : Singleton<LevelManager>
    {
        [SerializeField] private int roomToLoad;
        [SerializeField] private Animator fadeAnim;
        [SerializeField] private LevelObject level;
        [SerializeField] private GameObject poscig;
        [SerializeField] private Dialogue.DialogueObject tutorialDialouge;

        public GameObject Poscig => poscig;

        public GameObject exitDoor;
        
        public bool CanTravel { get; set; }
        public Room CurrentRoom { get; private set; }
        public RoomTrigger Trigger { private get; set; }
        public Animator Fade => fadeAnim;

        private bool started;
        private List<Room> _rooms = new List<Room>();

        private void Start()
        {
            LoadLevel(0, roomToLoad, 0,0.5f);
            CanTravel = true;
            StartCoroutine(Tutorial());
        }
        IEnumerator Tutorial()
        {
            yield return new WaitForSeconds(2.5f);
            Dialogue.DialogueManager.Instance.StartDialogue(tutorialDialouge.Dialogue);

        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.T) && PlayerManager.Instance.Movement.IsClimbing == false && CanTravel )
                ChangeTime();
        }

        public void ChangeTime()
        {
            if (CurrentRoom.id == CurrentRoom.idChange)
                return;

            if (started == true)
                fadeAnim.SetTrigger("Fade");

            var room = _rooms.SingleOrDefault(i => i.id == CurrentRoom.idChange);

            StartCoroutine(Fade());

            IEnumerator Fade()
            {
                yield return Helpers.GetWait(0.5f);

                if (room == null)
                {
                    room = Instantiate(level.Rooms[CurrentRoom.idChange], transform);
                    _rooms.Add(room.GetComponent<Room>());
                }

                if (CurrentRoom != null)
                    CurrentRoom.gameObject.SetActive(false);

                CurrentRoom = room.GetComponent<Room>();
                CurrentRoom.gameObject.SetActive(true);
                PlayerManager.Instance.SpawnPlayer();

                if (started == false)
                {
                    fadeAnim.enabled = true;
                    started = true;
                }
                else
                {
                    fadeAnim.SetTrigger("Fade");
                }
            }
        }

        public void LoadLevel(int door, int index, int doorNumbers, float time)
        {
            if (started == true)
            {
                fadeAnim.SetTrigger("Fade");
            }

            if (Trigger != null)
                Trigger.Enabled = false;

            var room = _rooms.SingleOrDefault(i => i.id == index);

            StartCoroutine(SceneFade(time));

            IEnumerator SceneFade(float time)
            {
                yield return Helpers.GetWait(time);

                if (Trigger != null)
                    Trigger.Enabled = true;

                if (room == null)
                {
                    room = Instantiate(level.Rooms[index], transform);
                    _rooms.Add(room.GetComponent<Room>());
                }


                for (var i = 0; i < room.transform.childCount; i++)
                {
                    if (room.transform.GetChild(i).CompareTag("DoorParent"))
                    {
                        exitDoor = room.transform.GetChild(i).transform.GetChild(room.transform.GetChild(i).transform.childCount-1).gameObject;
                    }
                }
                if (CurrentRoom != null)
                {
                    CurrentRoom.gameObject.SetActive(false);
                }

                CurrentRoom = room.GetComponent<Room>();
                CurrentRoom.gameObject.SetActive(true);

                PlayerManager.Instance.SpawnPlayer();
                CurrentRoom.StartPositions[door].Trigger();
                CharacterController2D.DisableMovement(true);
                if (started == false)
                {
                    fadeAnim.enabled = true;
                    started = true;
                }
                else
                {
                    fadeAnim.SetTrigger("Fade");
                }
                Managers.LevelManager.Instance.CanTravel = true;
                Trigger = null;
            }
        }
    }
}

using System;
using System.IO;
using System.Linq;
using PuzzledTime.Player;
using UnityEngine;

namespace PuzzledTime.Managers
{
    public class SaveManager
    {
        public void Save()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) +
                       $"\\Saved Games\\Puzzled Time";
            var currentDate = DateTime.Now;

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            path +=
                $"\\{currentDate.Hour}_{currentDate.Minute}_{currentDate.Day}_{currentDate.Month}_{currentDate.Year}.pt";
            using var fs = File.Create(path);
            using var save = new StreamWriter(fs);
            var position = PlayerManager.Instance.Player.transform.position;
            save.WriteLine(position.x);
            save.WriteLine(position.y);
            save.WriteLine(position.z);
        }

        public void Load(string path)
        {
            var lines = File.ReadLines(path).ToArray();
            
            PlayerManager.Instance.Player.transform.position = new Vector3(
                (float) Convert.ToDouble(lines[0]),
                (float) Convert.ToDouble(lines[1]),
                (float) Convert.ToDouble(lines[2]));
        }
    }
}    
    

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PuzzledTime.Utilities;

namespace PuzzledTime
{
    public class NewItem : Gui<NewItem>
    {
        bool m_isFree = true;
        Animator m_animator;

        private void Start()
        {
            m_animator = GetComponent<Animator>();
        }
        public void ShowNotification()
        {
            if (m_isFree)
            { 
                m_isFree = false;
                m_animator.SetTrigger("newItem");
                StartCoroutine(releaseAfterAnimation());
            }
        }
        IEnumerator releaseAfterAnimation()
        {
            yield return new WaitForSeconds(4f);
            m_isFree = true;
        }
    }
}

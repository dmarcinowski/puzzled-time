﻿using UnityEngine;

namespace PuzzledTime.Dialogue
{
    [CreateAssetMenu(fileName = "Dialogue", menuName = "ScriptableObjects/Dialogue")]
    public class DialogueObject : ScriptableObject
    {
        [SerializeField] private DialogueType type;
        [Space]
        [SerializeField] private string title;
        [Space]
        [SerializeField] [TextArea] private string[] sentences;

        public Dialogue Dialogue => new Dialogue(type, title, sentences);
    }
}

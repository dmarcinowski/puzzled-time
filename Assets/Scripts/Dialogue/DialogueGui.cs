﻿using PuzzledTime.Utilities;
using TMPro;
using UnityEngine;
using PuzzledTime.Movement;
using PuzzledTime.Managers;

namespace PuzzledTime.Dialogue
{
    public class DialogueGui : Gui<DialogueGui>
    {
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI sentenceText;

        public void SetName(string newName)
        {
            nameText.text = newName;
        }
     
        public void SetSentence(string sentence)
        {
            sentenceText.text = sentence;
        }

        public override void Show(bool value)
        {
            base.Show(value);
            CharacterController2D.canMove = !value;
        }
    }
}

﻿namespace PuzzledTime.Dialogue
{
    [System.Serializable]
    public struct Dialogue
    {
        public DialogueType type;
        public string name;
        public string[] sentences;

        public Dialogue(DialogueType type, string name, string[] sentences)
        {
            this.type = type;
            this.name = name;
            this.sentences = sentences;
        }
    }

    public enum DialogueType
    {
        Information,
        Dialogue,
        Persistant
    }
}

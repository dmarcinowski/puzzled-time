﻿using PuzzledTime.Interaction;
using UnityEngine;

namespace PuzzledTime.Dialogue
{
    public class DialogueTrigger : Interactable
    {
        [SerializeField] private DialogueObject dialogue;
        
        protected override string GizmoIcon => "DialogueTrigger.png";

        public override void Trigger()
        {
            InteractionGui.Instance.Disable(true);
            DialogueManager.Instance.StartDialogue(dialogue.Dialogue);
        }
    }
}

﻿using System.Collections.Generic;
using PuzzledTime.Utilities;
using PuzzledTime.Interaction;
using UnityEngine;
using UnityEngine.UI;
using PuzzledTime.Managers;

namespace PuzzledTime.Dialogue
{
    public class DialogueManager : Singleton<DialogueManager>
    {
        [Header("Triggered Dialogues")]
        [SerializeField] DialogueObject wrongItemDialogue;
        [SerializeField] DialogueObject itemDialogue;
        [SerializeField] DialogueObject puzzleDialogue;
        private readonly Queue<string> m_sentences = new Queue<string>();

        public bool IsEmpty { get; set; }

        public void Update()
        {
            if (DialogueGui.Instance.IsOpened)
            {
                if (Input.GetKeyDown(KeyCode.F))
                {
                    NextSentence();
                }

            }
        }

        public void StartDialogue(Dialogue dialogue)
        {
            m_sentences.Clear();
            foreach (var sentence in dialogue.sentences)
                m_sentences.Enqueue(sentence);
            IsEmpty = false;

            DialogueGui.Instance.SetName(dialogue.name);
            NextSentence();
            DialogueGui.Instance.Show(true);
        }

        public void NextSentence()
        {
            if (m_sentences.Count == 0)
            {
                DialogueGui.Instance.Show(false);
                InteractionGui.Instance.Disable(false);
                IsEmpty = true;
                return;
            }
            var sentence = m_sentences.Dequeue();
            DialogueGui.Instance.SetSentence(sentence);
        }


        public void WrongItemDialogue(int value)
        {
            if (value == 1)
                StartDialogue(wrongItemDialogue.Dialogue);
            else
                StartDialogue(itemDialogue.Dialogue);
        }
        public void PuzzleDialogue()
        {
            StartDialogue(puzzleDialogue.Dialogue);
        }
    }
}

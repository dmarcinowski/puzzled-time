﻿using PuzzledTime.Managers;
using UnityEngine;

namespace PuzzledTime.Player
{
    public class PlayerInteraction : MonoBehaviour
    {
        [SerializeField, NotNull] private Transform interactionPoint;

        private void OnTriggerEnter2D(Collider2D col)
        {
            InteractionManager.Instance.PlayerPoint = interactionPoint;
            InteractionManager.Instance.OnInteractionEnter(col);
        }

        private void OnTriggerStay2D(Collider2D col)
        {
            InteractionManager.Instance.OnInteractionEnter(col);
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            InteractionManager.Instance.OnInteractionExit();
        }
    }
}

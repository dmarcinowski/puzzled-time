﻿using PuzzledTime.Level;
using PuzzledTime.Managers;
using UnityEngine;

namespace PuzzledTime.Player
{
    public class StartPosition : MonoBehaviour
    {
        [SerializeField] private Room room;
        [SerializeField] private bool facingRight = true;

        public void Trigger()
        {
            PlayerManager.Instance.Player.transform.position = transform.position;
        }

        private void OnDrawGizmos() =>
            Gizmos.DrawIcon(transform.position, facingRight ? "StartPosition.png" : "EndPosition.png", true);
    }
}

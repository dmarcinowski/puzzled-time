﻿using PuzzledTime.Inventory;
using PuzzledTime.Movement;
using UnityEngine;

namespace PuzzledTime.Player
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private Animator animator;
        private readonly int m_walkAnimation = Animator.StringToHash("Walk");
        private readonly int m_jumpAnimation = Animator.StringToHash("Jump");
        private readonly int m_crouchAnimation = Animator.StringToHash("Crouch");
        private readonly int m_startAnimation = Animator.StringToHash("Start");
        private readonly int m_inventoryOpenedAnimation = Animator.StringToHash("InventoryOpened");

        [SerializeField] private CharacterController2D characterController;
        [SerializeField] private Rigidbody2D rb;
        [SerializeField] private float ladderSpeed = 10f;

        private float m_move;
        private bool m_crouch;
        private bool m_jump;
        private bool m_hitWall;

        private float m_vertical;
        private bool m_onLadder = false;
        private bool m_isClimbing = false;
        private LadderTrigger m_ladder = null;

        public bool IsClimbing => m_isClimbing;

        private void Start() 
        {
            CharacterController2D.canMove = false;
            Inventory.InventoryManager.Instance.canOpen = false;
        }

        private void Update()
        {
           if (CharacterController2D.canMove && m_onLadder == false)
           {
               if (InventoryGui.Instance.IsOpened)
               {
                   animator.SetBool(m_inventoryOpenedAnimation, true);
                   return;
               }

               if (animator.GetBool(m_inventoryOpenedAnimation))
                   animator.SetBool(m_inventoryOpenedAnimation, false);

               m_move = Input.GetAxisRaw("Horizontal");
               animator.SetFloat(m_walkAnimation, !m_hitWall ? Mathf.Abs(m_move) : 0f);

               if (Input.GetKeyDown(KeyCode.C) && !m_jump)
                   m_crouch = !m_crouch;

               if (Input.GetButtonDown("Jump") && !m_crouch)
               {
                   m_jump = true;
                   animator.SetBool(m_jumpAnimation, true);
                   InventoryGui.Instance.CanBeOpened = false;
               }
           }
           else if (CharacterController2D.canMove && m_onLadder)
           {
                m_vertical = Input.GetAxisRaw("Vertical");
                
                if (Mathf.Abs(m_vertical) > 0)
                    animator.Play("LadderClimb");
                else
                    animator.Play("LadderIdle");

                if (m_ladder.distanceToTop < 0.15f || m_ladder.distanceToBottom < 0.15f)
                {
                    m_ladder.Exit();
                    animator.Play("Idle");
                }
           }
           else
           {
               animator.SetFloat(m_walkAnimation, 0f);
               animator.SetBool(m_jumpAnimation, false);
               //Wyłączyłem to bo mi przy animacji początkowej przeszkadzało
               //animator.Play("Idle");
           }

        }
          
        private void FixedUpdate()
        {
            if (m_onLadder == false)
            {
                characterController.Move(m_move * Time.fixedDeltaTime, m_crouch, m_jump);
            }

            if (m_isClimbing == true)
            {
                rb.gravityScale = 0f;
                rb.velocity = new Vector2(0f, m_vertical * ladderSpeed);
            }
            else
            {
                rb.gravityScale = 7f;
            }
        }

        public void OnLandEvent()
        {
            m_jump = false;
            animator.SetBool(m_jumpAnimation, false);
            InventoryGui.Instance.CanBeOpened = true;
        }

        public void OnHitWallEvent(bool value)
        {
            m_hitWall = value;
        }

        public void OnCrouchEvent(bool value)
        {
            animator.SetBool(m_crouchAnimation, value);
        }

        public void ClimbLadder(LadderTrigger ladder)
        {
            m_onLadder = true;
            m_isClimbing = true;
            m_ladder = ladder;

            var initialOffset = 0.2f;
            if (ladder.distanceToTop < ladder.distanceToBottom)
                initialOffset *= -1;

            transform.position = new Vector2(ladder.transform.position.x, transform.position.y + initialOffset);

            var colliders = gameObject.GetComponents<Collider2D>();
            foreach (var col in colliders)
                col.enabled = false;
        }

        public void ExitLadder()
        {
            m_onLadder = false;
            m_isClimbing = false;
            m_ladder = null;

            var colliders = gameObject.GetComponents<Collider2D>();
            foreach (var col in colliders)
                col.enabled = true;
            rb.velocity = new Vector2(0f, 0f);
        }

        public void DisableStartMovement()
        {
            animator.SetBool(m_startAnimation, true);
            CharacterController2D.canMove = true;
            Inventory.InventoryManager.Instance.canOpen = true;
        }
    }
}

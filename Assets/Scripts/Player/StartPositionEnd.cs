﻿using PuzzledTime.Level;
using PuzzledTime.Managers;
using PuzzledTime.Utilities;
using PuzzledTime.Dialogue;
using PuzzledTime.Interaction;
using PuzzledTime.Movement;
using UnityEngine;
using System.Collections;

namespace PuzzledTime.Player
{
    public class StartPositionEnd : StartPosition
    {
        [SerializeField] private GameObject poscigLeft;
        [SerializeField] private GameObject poscigRight;
        [SerializeField] private GameObject zarzadca;
        [SerializeField] private Animator zarzadcaa;

        [SerializeField] private DialogueObject dialogue1;
        [SerializeField] private DialogueObject dialogue2;
        [SerializeField] private DialogueObject dialogue3;
        [SerializeField] private DialogueObject dialogue4;
        [SerializeField] private DialogueObject dialogue5;
        [SerializeField] private DialogueObject dialogue6;
        [SerializeField] private DialogueObject dialogue7;
        [SerializeField] private DialogueObject dialogue8;

        private Vector3 leftFinalPos = new Vector2(-6.25f, -3f);
        private Vector3 rightFinalPos = new Vector2(5f, -3f);

        private float dialogueTime = 2f;

        private bool move = false;
        private bool moveI = false;
        private Vector2 dest = Vector2.zero;
        private void Update()
        {
            if (move)
            {
                zarzadca.transform.position = Vector3.MoveTowards(zarzadca.transform.position, dest, 4f * Time.deltaTime);
                if (moveI)
                {
                    PlayerManager.Instance.Player.transform.position = Vector3.MoveTowards(PlayerManager.Instance.Player.transform.position, dest, 4f * Time.deltaTime);
                }
            }
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.gameObject.CompareTag("Player"))
                CharacterController2D.canMove = false;
            StartCoroutine(EndAnimation());
        }

        private IEnumerator EndAnimation()
        {
            DialogueManager.Instance.StartDialogue(dialogue1.Dialogue);
            while (DialogueManager.Instance.IsEmpty == false)
                yield return Helpers.GetWait(0.1f);
            LevelManager.Instance.Fade.SetTrigger("Fade");

            yield return Helpers.GetWait(0.5f);
            poscigLeft.SetActive(true);
            poscigRight.SetActive(true);

            yield return Helpers.GetWait(0.5f);
            LevelManager.Instance.Fade.SetTrigger("Fade");
            yield return Helpers.GetWait(0.5f);

            DialogueManager.Instance.StartDialogue(dialogue2.Dialogue);
            while (DialogueManager.Instance.IsEmpty == false)
                yield return Helpers.GetWait(0.1f);
            yield return Helpers.GetWait(0.5f);

            DialogueManager.Instance.StartDialogue(dialogue3.Dialogue);
            while (DialogueManager.Instance.IsEmpty == false)
                yield return Helpers.GetWait(0.1f);
            yield return Helpers.GetWait(0.5f);

            zarzadcaa.SetBool("walk", true);
            move = true; dest = new Vector2(2.5f, -3f);
            yield return Helpers.GetWait(3.5f);
            zarzadcaa.SetBool("walk", false);

            DialogueManager.Instance.StartDialogue(dialogue4.Dialogue);
            while (DialogueManager.Instance.IsEmpty == false)
                yield return Helpers.GetWait(0.1f);
            yield return Helpers.GetWait(0.5f);

            DialogueManager.Instance.StartDialogue(dialogue5.Dialogue);
            while (DialogueManager.Instance.IsEmpty == false)
                yield return Helpers.GetWait(0.1f);
            yield return Helpers.GetWait(0.5f);

            DialogueManager.Instance.StartDialogue(dialogue6.Dialogue);
            while (DialogueManager.Instance.IsEmpty == false)
                yield return Helpers.GetWait(0.1f);
            yield return Helpers.GetWait(0.5f);

            DialogueManager.Instance.StartDialogue(dialogue7.Dialogue);
            while (DialogueManager.Instance.IsEmpty == false)
                yield return Helpers.GetWait(0.1f);
            yield return Helpers.GetWait(0.5f);

            DialogueManager.Instance.StartDialogue(dialogue8.Dialogue);
            while (DialogueManager.Instance.IsEmpty == false)
                yield return Helpers.GetWait(0.1f);
            yield return Helpers.GetWait(0.5f);

            LevelManager.Instance.Fade.SetTrigger("Fade");
            zarzadcaa.SetBool("walk", true);
            zarzadca.GetComponent<SpriteRenderer>().flipX = false;
            PlayerManager.Instance.Player.GetComponent<SpriteRenderer>().flipX = false;
            move = true; moveI = true; dest = new Vector2(18f, -3f);
            yield return Helpers.GetWait(2f);
        }
    }
}

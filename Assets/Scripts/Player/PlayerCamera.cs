using PuzzledTime.Managers;
using PuzzledTime.Level;
using UnityEngine;

namespace PuzzledTime.Player
{
    public class PlayerCamera : MonoBehaviour
    {
        [SerializeField, NotNull] private Camera playerCamera;
        [SerializeField, Range(0f, 1f)] private float smoothing = 0.15f;

        [field: SerializeField]
        public bool Static { private get; set; }

        private Vector3 velocity = Vector3.zero;

        private Transform playerTransform;
        private Transform cameraTransform;
        private RoomBounds bounds;

        private float vertExtent;
        private float horzExtent;

        private void Start()
        {
            cameraTransform = playerCamera.transform;
            vertExtent = playerCamera.orthographicSize;
            horzExtent = vertExtent * playerCamera.aspect;
        }

        private void FixedUpdate()
        {
            if (playerTransform == null && PlayerManager.Instance.Player == null)
                return;
            if (playerTransform == null && PlayerManager.Instance.Player != null)
                playerTransform = PlayerManager.Instance.Player.transform;

            bounds = LevelManager.Instance.CurrentRoom.Bounds;

            var leftBound = bounds.left + horzExtent;
            var rightBound = bounds.right - horzExtent;

            var newPositionX = Mathf.Clamp(playerTransform.position.x, leftBound, rightBound);
            if (Mathf.Abs(bounds.left) + Mathf.Abs(bounds.right) < horzExtent * 2 || Static)
                newPositionX = (bounds.left + bounds.right) * 0.5f;

            var targetPostiton = new Vector3(newPositionX, cameraTransform.position.y, cameraTransform.position.z);
            cameraTransform.position = Vector3.SmoothDamp(cameraTransform.position, targetPostiton, ref velocity, smoothing);
        }
    }
}

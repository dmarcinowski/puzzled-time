﻿using System;
using PuzzledTime.Transitions;
using PuzzledTime.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace PuzzledTime.Inventory
{
    public class InventoryGui : Gui<InventoryGui>
    {
        [Header("Objects")]
        [SerializeField] private GameObject spinnerObject;
        [SerializeField] private GameObject slotsObject;
        
        public int SlotCount { get; private set; }
        private GameObject[] m_slots;

        private float m_rotation;
        private int m_rotationStep;
        
        protected override void Awake()
        {
            base.Awake();
            
            SlotCount = slotsObject.transform.childCount;
            
            m_slots = new GameObject[SlotCount];
            for (var i = 0; i < SlotCount; i++)
                m_slots[i] = slotsObject.transform.GetChild(i).gameObject;
            
            m_rotationStep = 360 / SlotCount;
        }

        protected override void Update()
        {
            base.Update();
            
            var newRotation = Quaternion.Lerp(
                spinnerObject.transform.rotation, 
                Quaternion.Euler(0f, 0f, m_rotation), 
                5f * Time.unscaledDeltaTime);
            
            spinnerObject.transform.rotation = newRotation;
            slotsObject.transform.rotation = newRotation;
        }

        public void Rotate(RotateDirection direction)
        {
            switch (direction) {
                case RotateDirection.Left:
                    m_rotation -= m_rotationStep;
                    break;
                case RotateDirection.Right:
                    m_rotation += m_rotationStep;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void AddItem(Item.Item item, int index)
        {
            m_slots[index].GetComponent<Image>().sprite = item.Sprite;
            m_slots[index].SetActive(true);
        }

        public void RemoveItem(int index)
        {
            m_slots[index].GetComponent<Image>().sprite = null;
            m_slots[index].SetActive(false);
        }

        public override void Show(bool value)
        {
            base.Show(value);
            TransitionGui.Instance.Transition(TransitionType.Gui, IsOpened);
            Managers.LevelManager.Instance.CanTravel = !value;
        }

        public void ReplaceItem(Item.Item oldItem, Item.Item newItem)
        {
            for (var i = 0; i < m_slots.Length; i++)
            {
                if (m_slots[i].GetComponent<Image>().sprite == oldItem.Sprite)
                {
                    m_slots[i].GetComponent<Image>().sprite = newItem.Sprite;
                }
            }
        }
    }

    public enum RotateDirection
    {
        Left,
        Right
    }
}

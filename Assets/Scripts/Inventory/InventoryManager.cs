using PuzzledTime.Dialogue;
using PuzzledTime.Inventory.Item;
using PuzzledTime.Transitions;
using PuzzledTime.Utilities;
using PuzzledTime.Managers;
using UnityEngine;

namespace PuzzledTime.Inventory
{
    public class InventoryManager : Singleton<InventoryManager>
    {
        public bool canOpen;
        [Header("Keybindings")]
        [SerializeField, SearchableEnum] private KeyCode toggleKey;
        [SerializeField, SearchableEnum] private KeyCode leftKey;
        [SerializeField, SearchableEnum] private KeyCode rightKey;
        
        private Inventory m_inventory;

        private int m_current;

        private void Start()
        {
            m_inventory = new Inventory(InventoryGui.Instance.SlotCount);
        }

        private void Update()
        {
            if (DialogueGui.Instance.IsOpened || PauseGui.Instance.IsOpened)
                return;

            if (Input.GetKeyDown(toggleKey) && canOpen && PlayerManager.Instance.Controller2D.Grounded)
            {
                InventoryGui.Instance.Show(!InventoryGui.Instance.IsOpened);
                TransitionGui.Instance.Transition(TransitionType.Gui, InventoryGui.Instance.IsOpened);
            }

            if (InventoryGui.Instance.IsOpened)
                ChangeItem();
            
            ItemGui.Instance.Show(!m_inventory.Item(m_current).IsEmpty);
        }

        private void ChangeItem()
        {
            if (Input.GetKeyDown(leftKey))
            {
                m_current = m_inventory.Previous(m_current);
                    
                InventoryGui.Instance.Rotate(RotateDirection.Left);
                if(!m_inventory.GetItem(m_current).IsEmpty)
                    ItemGui.Instance.SetInformation(m_inventory.Item(m_current));
            }
            else if (Input.GetKeyDown(rightKey) )
            {
                m_current = m_inventory.Next(m_current);
                    
                InventoryGui.Instance.Rotate(RotateDirection.Right);
                if (!m_inventory.GetItem(m_current).IsEmpty)
                    ItemGui.Instance.SetInformation(m_inventory.Item(m_current));
            }
        }

        public void AddItem(Item.Item item)
        {
            if (m_inventory.Item(m_current).IsEmpty)
            {
                InventoryGui.Instance.AddItem(item, m_current);
                m_inventory.Add(m_current, item);
                NewItem.Instance.ShowNotification();
            }
            else
            {
                for(var i=0; i<8; i++)
                {
                    if(m_inventory.Item(i).IsEmpty)
                    {
                        InventoryGui.Instance.AddItem(item, i);
                        m_inventory.Add(i, item);
                        NewItem.Instance.ShowNotification();
                        break;
                    }
                }
            }
            if (!m_inventory.Item(m_current).IsEmpty)
                ItemGui.Instance.SetInformation(m_inventory.Item(m_current));
        }

        public void RemoveItem()
        {
            InventoryGui.Instance.RemoveItem(m_current);
            m_inventory.Remove(m_current);
            
            if (!m_inventory.Item(m_current).IsEmpty)
                ItemGui.Instance.SetInformation(m_inventory.Item(m_current));
        }
        public void RemoveItem(int index)
        {
            InventoryGui.Instance.RemoveItem(index);
            m_inventory.Remove(index);
        }
        public void RemoveItem(Item.Item item)
        {
            var index = m_inventory.Remove(item);
            if (index == -1)
                return;

            InventoryGui.Instance.RemoveItem(index);
            if (!m_inventory.Item(m_current).IsEmpty)
                ItemGui.Instance.SetInformation(m_inventory.Item(m_current));
            m_inventory.Remove(index);
        }

        public void ReplaceItem(Item.Item oldItem, Item.Item newItem) 
        {
            m_inventory.ReplaceItem(oldItem, newItem);
            NewItem.Instance.ShowNotification();
            InventoryGui.Instance.ReplaceItem(oldItem, newItem);
            //if (m_inventory.Item(m_current).Id == oldItem.Id) TODO
                ItemGui.Instance.SetInformation(newItem);
        }

    
        public int GetItemId(int index)
        {
            return m_inventory.GetItemId(index);
        }
        public int GetCurrentId()
        {
            return m_inventory.Item(m_current).Id;
        }
        public int GetCurrentIndex()
        {
            return m_current;
        }
        public Item.Item GetCurrentitem()
        {
            return m_inventory.Item(m_current);
        }
    }
}

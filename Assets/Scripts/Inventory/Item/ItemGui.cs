﻿using PuzzledTime.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PuzzledTime.Inventory.Item
{
    public class ItemGui : Gui<ItemGui>
    {
        [SerializeField] private Image itemImage;
        [SerializeField] private TextMeshProUGUI itemName;
        [SerializeField] private TextMeshProUGUI itemDescription;

        public void SetInformation(Item item)
        {
            itemImage.sprite = item.Sprite;
            itemName.text = item.Name;
            itemDescription.text = item.Description;
            
            Show(true);
        }
    }
}

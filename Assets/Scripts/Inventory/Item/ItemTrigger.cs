﻿using PuzzledTime.Interaction;
using PuzzledTime.Managers;
using UnityEngine;

namespace PuzzledTime.Inventory.Item
{
    [RequireComponent(typeof(SpriteRenderer))]

    public class ItemTrigger : Interactable
    {
        [Header("Item information")]
        public ItemObject item;

        protected override string GizmoIcon => "ItemTrigger.png";

        public override void Trigger()
        {
            AudioManager.Instance.PlaySound("pick_up");
            InventoryManager.Instance.AddItem(item.Item);
            Destroy(gameObject);
        }
    }
}

﻿using JetBrains.Annotations;
using UnityEngine;

namespace PuzzledTime.Inventory.Item
{
    [System.Serializable]
    public struct Item
    {
        [CanBeNull] public int Id { get; set; }
        [CanBeNull] public string Name { get; set; }
        [CanBeNull] public Sprite Sprite { get; set; }
        [CanBeNull] public string Description { get; set; }
        
        public bool IsEmpty => Id == 0;
    }
}

﻿using UnityEngine;

namespace PuzzledTime.Inventory.Item
{
    [CreateAssetMenu(fileName = "Item", menuName = "ScriptableObjects/Item")]
    public class ItemObject : ScriptableObject
    {
        [SerializeField] private int itemId;
        [SerializeField] private string itemName;
        [SerializeField] private string itemDescription;
        [SerializeField] private Sprite itemSprite;

        public Sprite ItemSprite => itemSprite;

        public Item Item => new Item {
            Id = itemId,
            Name = itemName,
            Description = itemDescription,
            Sprite = itemSprite
        };
    }
}

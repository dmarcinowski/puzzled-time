﻿namespace PuzzledTime.Inventory
{
    [System.Serializable]
    public class Inventory
    {
        private readonly Item.Item[] m_slots;
        
        public Inventory(int itemCount)
        {
            m_slots = new Item.Item[itemCount];
        }
        
        public int NextEmpty { get; private set; }
        
        public int Next(int fromIndex)
        {
            if (fromIndex + 1 == m_slots.Length)
                return 0;
            
            return fromIndex + 1;
        }

        public int Previous(int fromIndex)
        {
            if (fromIndex - 1 < 0)
                return m_slots.Length - 1;
            
            return fromIndex - 1;
        }

        public void Add(int index, Item.Item item)
        {
            //NextEmpty = Next(NextEmpty);
            m_slots[index] = item;
        }
        
        public void Remove(int index)
        {
            NextEmpty = Previous(NextEmpty);
            m_slots[index] = new Item.Item();
        }

        public int Remove(Item.Item item)
        {
            for (var i = 0; i < m_slots.Length; i++)
            {
                if (m_slots[i].Id == item.Id)
                {
                    Remove(i);
                    return i;
                }
            }

            return -1;
        }

        public void ReplaceItem(Item.Item oldItem, Item.Item newItem) 
        {
            for (var i = 0; i < m_slots.Length; i++)
            {
                if (m_slots[i].Id == oldItem.Id)
                {
                    m_slots[i] = newItem;
                }
            }
        }
        public int GetItemId(int index)
        {
            return m_slots[index].Id;
        }
        public Item.Item GetItem(int index)
        {
            return m_slots[index];
        }
        public Item.Item Item(int index) => m_slots[index];
        public Item.Item NextItem(int current) => Item(Next(current));
        public Item.Item PreviousItem(int current) => Item(Previous(current));
    }
}

﻿using PuzzledTime.Interaction;
using PuzzledTime.Player;
using PuzzledTime.Managers;
using PuzzledTime.Movement;
using PuzzledTime.Utilities;
using UnityEngine;
using System.Collections;

namespace PuzzledTime.Level
{
    public class ResetTrigger : Interactable
    {
        [SerializeField] private StartPosition startPosition;

        protected override string GizmoIcon => "ResetTrigger.png";

        private Animator fadeAnimation;

        private void Start()
        {
            fadeAnimation ??= LevelManager.Instance.Fade;
        }

        public override void Trigger()
        {
            fadeAnimation.SetTrigger("Fade");
                
            StartCoroutine(Fade());

            IEnumerator Fade() 
            {
                CharacterController2D.canMove = false;
                yield return Helpers.GetWait(0.5f);

                startPosition.Trigger();

                Enabled = true;
                CharacterController2D.canMove = true;
            }
        }
    }
}

﻿using PuzzledTime.Interaction;
using PuzzledTime.Managers;
using PuzzledTime.Player;
using UnityEngine;
using UnityEditor;
using PuzzledTime.Movement;
using System.Collections;
using System.Collections.Generic;
using PuzzledTime.Inventory;
using PuzzledTime.Dialogue;

namespace PuzzledTime.Level
{
    public class RoomTrigger : Interactable
    {
        public int[] listOfItem;

        [SerializeField] bool locked;
        [SerializeField] bool PuzzleSolved = true;

        [SerializeField] private int doorId;
        [SerializeField] private int roomId;
        [SerializeField] private StartPosition startPosition;

        [SerializeField] private DialogueObject customDialouge;

        [SerializeField] private float soundExitTime = 0.5f;

        [SerializeField] private string audioClip = "otwieranie_drzwi_v2";

        private void Check(int index)
        {
            for (var i = 0; i < listOfItem.Length; i++)
            {
                if (index == listOfItem[i])
                {
                    listOfItem[i] = 0;
                    InventoryManager.Instance.RemoveItem();
                }

            }
            CheckCondition();
        }

        private void CheckCondition()
        {
            foreach (var element in listOfItem)
            {
                if (element != 0)
                {
                    DialogueManager.Instance.WrongItemDialogue(0);
                    return;
                }

            }
            if (PuzzleSolved)
                locked = false;
            else
            {
                if (customDialouge == null)
                    DialogueManager.Instance.PuzzleDialogue();
                else
                    DialogueManager.Instance.StartDialogue(customDialouge.Dialogue);
            }         
        }

        protected override string GizmoIcon => "DoorTrigger.png";
        
        public override void Trigger()
        {
            var currentItem = InventoryManager.Instance.GetCurrentId();
            Check(currentItem);
            if (!locked)
            {
                Managers.LevelManager.Instance.CanTravel = false;
                CharacterController2D.DisableMovement(false);
                AudioManager.Instance.PlaySound(audioClip);
                LevelManager.Instance.Trigger = gameObject.GetComponent<RoomTrigger>();
                if (roomId != 0)
                    LevelManager.Instance.LoadLevel(doorId, roomId, 1, soundExitTime);
                else
                    LevelManager.Instance.LoadLevel(doorId, roomId, 0, soundExitTime);
            }
            else 
            {
                InteractionGui.Instance.Disable(true);
                DialogueManager.Instance.StartDialogue(customDialouge.Dialogue);
            }
        }

        public void SolvePuzzle()
        {
            PuzzleSolved = true;
        }
    }
}

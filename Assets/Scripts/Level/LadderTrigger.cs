using PuzzledTime.Interaction;
using PuzzledTime.Managers;
using PuzzledTime.Player;
using PuzzledTime.Inventory.Item;
using PuzzledTime.Inventory;
using UnityEngine;

namespace PuzzledTime
{
    public class LadderTrigger : Interactable
    {
        [NotNull] public Transform top;
        [NotNull] public Transform bottom;
        [SerializeField] ItemObject[] itemList; 
        
        private PlayerMovement m_movement;
        private Transform m_playerGroundCheck;
        public float distanceToTop { get; private set; }
        public float distanceToBottom { get; private set; }

        public bool requied;

        protected override string GizmoIcon => "LadderTrigger.png";

        private void Start()
        {
            m_movement = PlayerManager.Instance.Movement;
            m_playerGroundCheck = PlayerManager.Instance.Controller2D.GroundCheck;
        }

        private void Update()
        {
            distanceToTop = Vector3.Distance(top.position, m_playerGroundCheck.position);
            distanceToBottom = Vector3.Distance(bottom.position, m_playerGroundCheck.position);
        }

        public void Exit()
        {
            m_movement.ExitLadder();
            Enabled = true;
        }
        private bool HasItem()
        {
            foreach(var item in itemList)
            {
                for(var i = 0; i < 8; i++)
                {
                    if (item.Item.Id == InventoryManager.Instance.GetItemId(i))
                        return true;
                }
            }    
            return false;
        }
        public override void Trigger()
        {
            if (HasItem() && requied || !requied)
            {
                m_movement.ClimbLadder(this);
                distanceToTop = Vector3.Distance(top.position, m_playerGroundCheck.position);
                distanceToBottom = Vector3.Distance(bottom.position, m_playerGroundCheck.position);
                Enabled = false;
            }
            else
            {
                InteractionGui.Instance.Disable(true);
            }
        }

        protected override void OnDrawGizmos()
        {
            base.OnDrawGizmos();
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(top.position, 0.15f);
            Gizmos.DrawWireSphere(bottom.position, 0.15f);
        }
    }
}

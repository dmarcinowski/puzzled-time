namespace PuzzledTime.Level
{
    [System.Serializable]
    public struct RoomBounds
    {
        public float top, bottom, left, right;
    }
}

﻿using UnityEngine;

namespace PuzzledTime.Level
{
    [CreateAssetMenu(fileName = "Level", menuName = "ScriptableObjects/Level")]
    public class LevelObject : ScriptableObject
    {
        [SerializeField, NewLabel("Room", "Element"), ReorderableList]
        private Room[] rooms;
        
        public Room[] Rooms { get => rooms; set => rooms = value; }
    }
}

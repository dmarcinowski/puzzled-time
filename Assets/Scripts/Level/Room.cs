﻿using PuzzledTime.Player;
using UnityEngine;

namespace PuzzledTime.Level
{
    public class Room : MonoBehaviour
    {
        [SerializeField] private StartPosition[] startPositions;
        [SerializeField] private RoomBounds bounds;

        public StartPosition[] StartPositions => startPositions;
        public RoomBounds Bounds => bounds;
        public int id;
        public int idChange;

        public void OnDrawGizmosSelected()
        {
            var tl = new Vector3(bounds.left  ,bounds.top);
            var tr = new Vector3(bounds.right ,bounds.top);
            var bl = new Vector3(bounds.left  ,bounds.bottom);
            var br = new Vector3(bounds.right ,bounds.bottom);

            Gizmos.color = Color.green;
            Gizmos.DrawLine(tl, tr); // top
            Gizmos.DrawLine(tl, bl); // left
            Gizmos.DrawLine(bl, br); // bottom
            Gizmos.DrawLine(br, tr); // right
        }
    }
}
